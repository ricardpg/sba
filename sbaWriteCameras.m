%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Write the Monocular/Stereo Sparse Bundle Adjustement
%                  Cameras structure.
%
%  File          : sbaWriteCameras.m
%  Date          : 30/01/2007 - 13/02/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  sbaWriteCameras Write the content of the Monocular/Stereo Bundle Adjustement
%                  Cameras matrix to a file.
%
%      sbaWriteCameras ( Cameras, FileName )
%
%     Input Parameters:
%      Cameras: 7xK camera positions (qr, q1, q2, q3, X, Y, Z).
%      FileName: String containing the full file path where the data must be
%                stored.
%
%     Output Parameters:
%
%

function sbaWriteCameras ( Cameras, FileName )
  % Test the input parameters
  error ( nargchk ( 2, 2, nargin ) );
  error ( nargoutchk ( 0, 0, nargout ) );

  [cnp, nframes] = size ( Cameras );
  if ~isnumeric ( Cameras ) || cnp ~= 7 || nframes <= 0;
    error ( 'MATLAB:sbaWriteCameras:Input', 'Calibration must be a 7xK real matrix!' );
  end

  FileFID = fopen ( FileName, 'w' );
  if FileFID < 0; error ( 'MATLAB:sbaWriteCameras:Input', [ 'Cannot open "' FileName '" to write Cameras structure!' ] ); end

  % Write the camera poses data
  for i = 1 : nframes;
    fprintf ( FileFID, '%.16f\t%.16f\t%.16f\t%.16f\t%.16f\t%.16f\t%.16f\n', ...
              Cameras(1,i), Cameras(2,i), Cameras(3,i), Cameras(4,i), ...
              Cameras(5,i), Cameras(6,i), Cameras(7,i) );
  end

  fclose ( FileFID );
end
