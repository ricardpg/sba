%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Check the Sparse Bundle Adjustement paratemeter structure.
%
%  File          : sbaCheckParameters.m
%  Date          : 19/01/2007 - 19/02/2009
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  sbaCheckParameters Check the Sparse Bundle Adjustement paratemeter structure.
%                     a set of 3D points.
%
%      SbaParametersR = sbaCheckParameters ( SbaParameters )
%
%     Input Parameters:
%      SbaParameters: Structure containing the Sparse Bundle Adjustement
%                     configuration parameters.
%                     This structure contain the following fields:
%        - Verbose: Boolean specifying whether to produce result or not.
%        - Type: String containing the type of Bundle Adjustement to be
%                performed:
%                   BA_MOTSTRUCT     : Minimize the Motion and the Structure.
%                   BA_MOT           : Minimize the Motion only.
%                   BA_STRUCT        : Minimize the Structure only.
%                   BA_MOT_MOTSTRUCT : Minimize the Motion and possibly Motion
%                                      & Structure in a 2nd pass.
%        - AnalyticJac: Boolean indicating if the jacobians must be computed
%                       analytically or not.
%        - InitialMU: Scaling factor for stopping thresholds.
%        - TermThresh1: Stopping threshold: ||J^T e||_inf (Largest value of the right part of the linear equation being solved at each iteration "A * x" = J^T * e).
%        - TermThresh2: Stopping threshold: ||dp||_2 (Norm-2 of gradient of parameter vector).
%        - TermThresh3: Stopping threshold: ||e||_2 (Norm-2 of the error vector).
%        - TermThresh4: Stopping threshold: ||e||_2-||e_new||_2)/||e||_2 (Relative improvement of the norm of the errors).
%        - MeanReprojecError: ||e||_2 = Average Reprojection error stopping threshold.
%        - Expert: Boolean to set the expert minimization.
%        - NumConstFrames: Number of contant camera poses that will not be
%                          optimized.
%        - MaxIterations: Maximum number of iterations.
%
%     Output Parameters:
%      SbaParametersR: Output right Sparse Bundle Adjustment Parameter
%                      structure.
%

function SbaParametersR = sbaCheckParameters ( SbaParameters )
  % Test the input parameters
  error ( nargchk ( 0, 1, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % ~~~~~~~~~~ Default Parameter
  DefaultVerbose = false;           % Verbosity activated
  DefaultType = int8(0);            % Default Type of minimization (BA_MOTSTRUC)
  DefaultAnalyticJac = true;        % Compute Jacobians analytically?
  DefaultInitialMu = 1E-03;         % Initial Mu (Scaling Factor)
  DefaultTermThresh1 = 1E-12;       % Default Termination Threshold 1
  DefaultTermThresh2 = 1E-12;       % Default Termination Threshold 2
  DefaultTermThresh3 = 1E-12;       % Default Termination Threshold 3
  DefaultTermThresh4 = 0;           % Default Termination Threshold 4
  DefaultExpert = true;             % Expert Mode Minimization.
  DefaultNumConstFrames = 1;        % Number Of Constant Frames (not optimized)
  DefaultMaxIterations = 150;       % Maximum Number of iterations

  % ~~~~~~~~~~ Checking all SbaParameters
  % If SbaParameters is omited => Use default values
  if nargin == 0
      SbaParameters = 1;
  end

  if isstruct ( SbaParameters ),
    % Check individually for the default value
    if isfield ( SbaParameters, 'Verbose' ),
      if islogical ( SbaParameters.Verbose ),
        SbaParametersR.Verbose = SbaParameters.Verbose;
      else
         warning ( 'MATLAB:sbaCheckParameters:Input', 'Incorrect Verbose value, default will be used!' );
         SbaParametersR.Verbose = DefaultVerbose;
      end
    else
      SbaParametersR.Verbose = DefaultVerbose;
    end

    if isfield ( SbaParameters, 'Type' ),
      if ( isscalar ( SbaParameters.Type ) && SbaParameters.Type == 0 ) || ...
         ( ischar ( SbaParameters.Type ) && strcmpi ( SbaParameters.Type, 'BA_MOTSTRUCT' ) ),
        SbaParametersR.Type = int8(0);
      elseif ( isscalar ( SbaParameters.Type ) && SbaParameters.Type == 1 ) || ...
             ( ischar ( SbaParameters.Type ) && strcmpi ( SbaParameters.Type, 'BA_MOT' ) ),
        SbaParametersR.Type = int8(1);
      elseif ( isscalar ( SbaParameters.Type ) && SbaParameters.Type == 2 ) || ...
             ( ischar ( SbaParameters.Type ) && strcmpi ( SbaParameters.Type, 'BA_STRUCT' ) ),
        SbaParametersR.Type = int8(2);
      elseif ( isscalar ( SbaParameters.Type ) && SbaParameters.Type == 3 ) || ...
             ( ischar ( SbaParameters.Type ) && strcmpi ( SbaParameters.Type, 'BA_MOT_MOTSTRUCT' ) ),
        SbaParametersR.Type = int8(3);
      else
         warning ( 'MATLAB:sbaCheckParameters:Input', 'Incorrect minimization type, BA_MOTSTRUC will be used!' );
         SbaParametersR.Type = DefaultType;
      end
    else
      SbaParametersR.Type = DefaultType;
    end

    if isfield ( SbaParameters, 'AnalyticJac' ),
      if islogical ( SbaParameters.AnalyticJac ),
        SbaParametersR.AnalyticJac = SbaParameters.AnalyticJac;
      else
         warning ( 'MATLAB:sbaCheckParameters:Input', 'Incorrect Jacobian computation type, default will be used!' );
         SbaParametersR.AnalyticJac = DefaultAnalyticJac;
      end
    else
      SbaParametersR.AnalyticJac = DefaultAnalyticJac;
    end

    if isfield ( SbaParameters, 'InitialMu' ),
      if isscalar ( SbaParameters.InitialMu ),
        SbaParametersR.InitialMu = SbaParameters.InitialMu;
      else
         warning ( 'MATLAB:sbaCheckParameters:Input', 'Incorrect Initial Mu, default will be used!' );
         SbaParametersR.InitialMu = DefaultInitialMu;
      end
    else
      SbaParametersR.InitialMu = DefaultInitialMu;
    end

    if isfield ( SbaParameters, 'TermThresh1' ),
      if isscalar ( SbaParameters.TermThresh1 ),
        SbaParametersR.TermThresh1 = SbaParameters.TermThresh1;
      else
         warning ( 'MATLAB:sbaCheckParameters:Input', 'Incorrect Termination Threshold 1, default will be used!' );
         SbaParametersR.TermThresh1 = DefaultTermThresh1;
      end
    else
      SbaParametersR.TermThresh1 = DefaultTermThresh1;
    end

    if isfield ( SbaParameters, 'TermThresh2' ),
      if isscalar ( SbaParameters.TermThresh2 ),
        SbaParametersR.TermThresh2 = SbaParameters.TermThresh2;
      else
         warning ( 'MATLAB:sbaCheckParameters:Input', 'Incorrect Termination Threshold 2, default will be used!' );
         SbaParametersR.TermThresh2 = DefaultTermThresh2;
      end
    else
      SbaParametersR.TermThresh2 = DefaultTermThresh2;
    end

    if isfield ( SbaParameters, 'TermThresh3' ),
      if isscalar ( SbaParameters.TermThresh3 ),
        SbaParametersR.TermThresh3 = SbaParameters.TermThresh3;
      else
         warning ( 'MATLAB:sbaCheckParameters:Input', 'Incorrect Termination Threshold 3, default will be used!' );
         SbaParametersR.TermThresh3 = DefaultTermThresh3;
      end
    else
      SbaParametersR.TermThresh3 = DefaultTermThresh3;
    end
    
    if isfield ( SbaParameters, 'TermThresh4' ),
      if isscalar ( SbaParameters.TermThresh4 ),
        SbaParametersR.TermThresh4 = SbaParameters.TermThresh4;
      else
         warning ( 'MATLAB:sbaCheckParameters:Input', 'Incorrect Termination Threshold 4, default will be used!' );
         SbaParametersR.TermThresh4 = DefaultTermThresh4;
      end
    else
      SbaParametersR.TermThresh4 = DefaultTermThresh4;
    end
    
    if isfield ( SbaParameters, 'Expert' ),
      if islogical ( SbaParameters.Expert ),
        SbaParametersR.Expert = SbaParameters.Expert;
      else
         warning ( 'MATLAB:sbaCheckParameters:Input', 'Incorrect Expert mode, default will be used!' );
         SbaParametersR.Expert = DefaultExpert;
      end
    else
      SbaParametersR.Expert = DefaultExpert;
    end

    if isfield ( SbaParameters, 'NumConstFrames' ),
      if isscalar ( SbaParameters.NumConstFrames ),
        SbaParametersR.NumConstFrames = SbaParameters.NumConstFrames;
      else
         warning ( 'MATLAB:sbaCheckParameters:Input', 'Incorrect number of constant frames, default will be used!' );
         SbaParametersR.NumConstFrames = DefaultNumConstFrames;
      end
    else
      SbaParametersR.NumConstFrames = DefaultNumConstFrames;
    end
    
    if isfield ( SbaParameters, 'MaxIterations' ),
      if isscalar ( SbaParameters.MaxIterations ),
        SbaParametersR.MaxIterations = SbaParameters.MaxIterations;
      else
         warning ( 'MATLAB:sbaCheckParameters:Input', 'Incorrect maximum number of iterations, default will be used!' );
         SbaParametersR.MaxIterations = DefaultMaxIterations;
      end
    else
      SbaParametersR.MaxIterations = DefaultMaxIterations;
    end
    
  elseif isscalar ( SbaParameters )

    if SbaParameters ~= 0       % Load Default parameters
      SbaParametersR.Verbose = DefaultVerbose;
      SbaParametersR.Type = DefaultType;
      SbaParametersR.AnalyticJac = DefaultAnalyticJac;
      SbaParametersR.InitialMu = DefaultInitialMu;
      SbaParametersR.TermThresh1 = DefaultTermThresh1;
      SbaParametersR.TermThresh2 = DefaultTermThresh2;
      SbaParametersR.TermThresh3 = DefaultTermThresh3;
      SbaParametersR.TermThresh4 = DefaultTermThresh4;
      SbaParametersR.Expert = DefaultExpert;
      SbaParametersR.NumConstFrames= DefaultNumConstFrames;
      SbaParametersR.MaxIterations = DefaultMaxIterations;
    else                         % No msba
      SbaParametersR = 0;
    end
  else
    error ( 'MATLAB:sbaCheckParameters:Input', ...
          [ 'Incorrect SbaParameters input: can be 0, 1 or structure \n', ...
            'with Verbose, Type, AnalyticJac, InitialMu, TermThresh1, TermThres2, TermThres3, TermThres4, Expert, NumConstFrames and MaxIterations fields!' ] );
  end
end
