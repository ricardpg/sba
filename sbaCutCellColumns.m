%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Cut columns with an empty cell in a 2D cell array.
%
%  File          : sbaCutCellColumns.m
%  Date          : 30/01/2007 - 14/02/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  sbaCutCellColumns Reduce the size of a cell array erasing any column
%                    with one or more empty cells.
%
%      CellO = sbaCutCellColumns ( CellI [, MinNumber, MaxNumber] )
%
%     Input Parameters:
%      CellI: A MxN cell array.
%      MinNumber: Minimum number of columns to accept a cell-column.
%      MaxNumber: Maximum number of columns to accept a cell-column.
%                 NOTE: If a single bound is given it will be considered
%                       the MinNumber.
%
%     Output Parameters:
%      CellR: A MxN cell in which there is no empty cells.
%
%

function CellO = sbaCutCellColumns ( CellI, MinNumber, MaxNumber )
  % Test the input parameters
  error ( nargchk ( 1, 3, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % Check the Points Structure
  [M, N] = size ( CellI );
  if ~iscell ( CellI ) || M <= 0 || N <= 0;
    error ( 'MATLAB:sbaCutPoints:Input', 'CellI must be a MxN cell-array!' );
  end

  % Count empty before to don't grow the output cell-array.
  Size = 0;
  IsEmpty = ones ( 1, N );
  for i = 1 : N;
    EmptyColumn = false;
    j = 1;
    while j <= M && ~EmptyColumn;
      % Test if it is empty
      EmptyColumn = isempty ( CellI{j, i} );
      % If it is not empty, test if it is in bounds.
      if ~EmptyColumn && nargin > 1;
        if nargin == 2;
          EmptyColumn = size ( CellI{j,i}, 2 ) < MinNumber;
        elseif nargin == 3;
          EmptyColumn = size ( CellI{j,i}, 2 ) < MinNumber || ...
                        size ( CellI{j,i}, 2 ) > MaxNumber;
        end
      end
      if ~EmptyColumn; j = j + 1; end;
    end
    if ~EmptyColumn; Size = Size + 1; IsEmpty(i) = 0; end;
  end

  % Create output cell
  CellO = cell ( M, Size );
  
  % If there is data => Copy
  if Size > 0;
    k = 1;
    for i = 1 : N;
      % Is is not empty => Copy Data
      if ~IsEmpty(i);
        for j = 1 : M;
          CellO{j,k} = CellI{j,i};
        end
        k = k + 1;
      end
    end
  end
end
