%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Write the Sparse Bundle Adjustement Points structure.
%
%  File          : sbaWritePoints.m
%  Date          : 30/01/2007 - 14/10/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  sbaWritePoints Write the content of Points structure that contains the 3D
%                 points and the image projections to the given file.
%
%      sbaWritePoints ( Points, FileName, IsStereo )
%
%     Input Parameters:
%      Points: See msba.m
%      FileName: String containing the full file path where the data must be
%                stored.
%      IsStereo: If true, the Stereo is taken into account otherwise
%                monouclar case is assumed. In Stereo, Points structure
%                must have 2 x 2D points in the rows of the cell.
%
%     Output Parameters:
%
%

function sbaWritePoints ( Points, FileName, IsStereo )
  % Test the input parameters
  error ( nargchk ( 3, 3, nargin ) );
  error ( nargoutchk ( 0, 0, nargout ) );

  % Check the Point structure content
  sbaCheckPoints ( Points, false, IsStereo );
  numpts3D = size ( Points, 2 );

  FileFID = fopen ( FileName, 'w' );
  if FileFID < 0; error ( 'MATLAB:sbaWritePoints:Input', [ 'Cannot open "' FileName '" to write Points structure!' ] ); end

  % Write the projections and frames data
  for i = 1 : numpts3D;
    n = size ( Points{2,i}, 2 );
    fprintf ( FileFID, '%.16f\t%.16f\t%.16f\t%d\t', ...
              Points{1,i}(1), Points{1,i}(2), Points{1,i}(3), n );
    for j = 1 : n - 1;
      fprintf ( FileFID, '%d\t%.16f\t%.16f\t', int32(Points{2,i}(1,j)) - 1, ...
                Points{2,i}(2,j), Points{2,i}(3,j) );
      if IsStereo;
        fprintf ( FileFID, '%.16f\t%.16f\t', Points{2,i}(4,j), Points{2,i}(5,j) );
      end
    end
    % Last one changing the last Tabulator for Return character.
    fprintf ( FileFID, '%d\t%.16f\t%.16f', int32(Points{2,i}(1,n)) - 1, ...
              Points{2,i}(2,n), Points{2,i}(3,n) );
    if IsStereo;
      fprintf ( FileFID, '\t%.16f\t%.16f', Points{2,i}(4,n), Points{2,i}(5,n) );
    end
    fprintf ( FileFID, '\n');
  end

  fclose ( FileFID );
end
