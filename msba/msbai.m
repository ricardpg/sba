%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Monocular Sparse Bundle Adjustement Wrapper for Matlab.
%
%  File          : msbai.m
%  Date          : 17/01/2007 - 09/06/2009
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%                  - If the external msbaiMx ends up with an error, this will
%                    stop Matlab.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2009 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  msbai Monocular Sparse Bundle Adjustement to refine the motion and the
%        structure of a set of 3D points.
%
%      [CamerasR PointsR ReprError] = msbai ( Calibration, Motions, Points[, SbaParameters] )
%
%     Input Parameters:
%      Calibration: 3x3 Intrinsic parameters camera matrix.
%      Cameras: 7xN camera positions expressed as one Quaterion for the
%               rotation and X, Y, Z for position
%      Points: See msba.m
%      MSbaParameters: See sbaCheckParameters.m
%
%     Output Parameters:
%      CamerasR: 7xN refined camera motions.
%      PointsR: 3xM refined set of 3D points.
%      ReprError: 2x1 vector containing the initial and final reprojection
%                 error respectively.
%
%

function [CamerasR PointsR ReprError] = msbai ( Calibration, Cameras, Points, SbaParameters )
  % Test the input parameters
  error ( nargchk ( 3, 4, nargin ) );
  error ( nargoutchk ( 2, 3, nargout ) );

  % If don't provided, use default ones
  if nargin == 3; SbaParameters = 1; end

  % Check sba input parameters
  SbaParameters = sbaCheckParameters ( SbaParameters );

  [Kr, Kc] = size ( Calibration );
  if Kr ~= 3 || Kc ~= 3;
    error ( 'MATLAB:msbai:Input', 'Calibration must be a 3x3 double matrix!' );
  end

  [Qr, Qc] = size ( Cameras );
  if Qr ~= 7 || Qc <= 0;
    error ( 'MATLAB:msbai:Input', 'Cameras must be a 7xN double matrix containing (q1, q2, q3, q4, X, Y, Z) for each camera!' );
  end

  [Cr, Cc] = size ( Points );
  if ~iscell ( Points ) || Cr ~= 2 || Cc <= 0;
    error ( 'MATLAB:msbai:Input', [ 'Points must be a 2xK cell-array containing on each column:\n' ...
                                    '  { (X,Y,Z) * 1 Time; (Frame Number, X-2D, Y-2D) * T Times }!' ] );
  end

  % Call the C-Mex function
  [CamerasR PointsR ReprError] = msbaiMx ( Calibration, Cameras, Points, SbaParameters.Type, ...
                                           SbaParameters.AnalyticJac, ...
                                           SbaParameters.InitialMu, SbaParameters.TermThresh1, ...
                                           SbaParameters.TermThresh2, SbaParameters.TermThresh3, SbaParameters.TermThresh4, ...
                                           SbaParameters.Expert, SbaParameters.NumConstFrames, SbaParameters.MaxIterations, ...
                                           SbaParameters.Verbose );
end
