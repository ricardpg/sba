/*
  Autor(s)      : Jordi Ferrer Plana
  e-mail        : jferrerp@eia.udg.edu
  Branch        : Computer Vision

  Working Group : Underwater Vision Lab
  Project       : -

  Homepage      : http://porcsenglar.udg.edu

  Module        : Generic error functions.

  File          : errorMx.h
  Date          : 22/01/2007 - 31/03/2009

  Compiler      : MATLAB >= 7.0 & g++ >= 4.x
  Libraries     : -

  Notes         : - File written using ISO-8859-1 encoding.

 -----------------------------------------------------------------------------

  Copyright (C) 2005-2006 by Jordi Ferrer Plana

  This source code is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This source code is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
  more details.

 -----------------------------------------------------------------------------
*/

#ifndef __ERRORMX_H__
#define __ERRORMX_H__

#include <sba_types.h>

#define STR_ERROR_BUFFER_SIZE 4096   // Should be enought

// Function to print mex error using printf() syntax.
//
void printfmexErrMsgTxt ( const char *Format, ... );

// Function to print mex warning using printf() syntax.
//
void printfmexWarnMsgTxt ( const char *Format, ... );

// Cut Src string into and smaller one containing part of the beggining and
// part of the end in the case that there is not enought space to store the
// full one. Size does not include the zero. The real memory space must be
// DstSize+1 or SrcSize+1.
//
void resumeStr ( char *Dst, const suint DstSize,
                 const char *Src, const suint SrcSize );

#endif
