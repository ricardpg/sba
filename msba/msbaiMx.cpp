/*
  Autor(s)      : Jordi Ferrer Plana
  e-mail        : jferrerp@eia.udg.edu
  Branch        : Computer Vision

  Working Group : Underwater Vision Lab
  Project       : -

  Homepage      : http://porcsenglar.udg.edu

  Module        : Monocular Bundle Adjustement Wrapper for Matlab.

  File          : msbaiMx.cpp
  Date          : 16/01/2007 - 09/06/2009

  Compiler      : MATLAB >= 7.0 & g++ >= 4.x
  Libraries     : -

  Notes         : - File written using ISO-8859-1 encoding.

 -----------------------------------------------------------------------------

  Copyright (C) 2005-2009 by Jordi Ferrer Plana

  This source code is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This source code is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
  more details.

 -----------------------------------------------------------------------------
*/

#include <ctime>            // STL: Time functions
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <iostream>

#include <mex.h>            // Matlab Mex Functions

#include <sba.h>            // Sparse Bundle Adjustment
#include <eucsbademo.h>
#include <sba_types.h>

#include "msbaChecks.h"     // Structure Checkings
#include "quat.h"           // Quaternion normalization

#include "bin/msba.h"       // Monocular Sparse Bundle Adjustment
#include "errorMx.h"        // To Print out Errors

#define CLOCKS_PER_MSEC (CLOCKS_PER_SEC/1000.0)
#define MSBA_MAX_REPROJ_ERROR 0.1  // Threshold to refine motion+structure after structure

// Common string to some output messages
#define ERROR_HEADER       "MATLAB:msbaiMx:Input\n"
#define CELL_CONTENT_MSG1  "{ (X,Y,Z) * 1 Time; (Frame Number, x_2D, y_2D) * T Times }!"

// Identify the input parameters by it's index
enum { CALIBRATION=0, CAMERAS, POINTS, TYPE, ANALYTICJAC, INITIALMU, TERMTHRESH1,
       TERMTHRESH2, TERMTHRESH3, TERMTHRESH4, EXPERT, NUMCONSTFRAMES, MAXITERATIONS, VERBOSE };
// Identify the output parameters by it's index
enum { CAMERASR=0, POINTSR, REPRERROR };


void mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  // Constants
  const sint pnp = 3;           // 3D Point number of elements (X, Y, Z)
  const sint cnp = 6;           // Pose number of elemets ( q2, q3, q4, X, Y, Z )
  const sint inpcnp = cnp+1;    // Pose number of elements ( q1, q2, q3, q4, X, Y, Z )
  const sint mnp = 2;           // 2D Point number of elements (x, y)
  const sint VerboseBundle = 0; // Verbosity inside the bundle

  // Parameter checks
  if ( nlhs != 3 || nrhs != 14 )
    mexErrMsgTxt ( ERROR_HEADER
                   "Usage: [CamerasR PointsR ReprError] = msbaiMx ( Calibration, Cameras, Points, Type, AnalyticJac,\n"
                   "                                                InitialMu, TermThresh1, TermThresh2, TermThresh3, TermThresh4,\n"
                   "                                                Expert, NumConstFrames, MaxIterations, Verbose )" );

  // Test the Camera Poses Input parameter
  if ( mxGetNumberOfDimensions ( prhs[CAMERAS] ) != 2 || !mxIsDouble ( prhs[CAMERAS] ) )
    mexErrMsgTxt ( ERROR_HEADER "Camera poses must be a 7xN double matrix!" );

  sint nframes = mxGetN ( prhs[CAMERAS] );  // Number of cameras

  // Motion Parameters must be 7: q1, q2, q3, q4, X, Y, Z. Number of cameras must be >= 0
  if ( mxGetM ( prhs[CAMERAS] ) != inpcnp || nframes <= 0 )
    mexErrMsgTxt ( ERROR_HEADER "Camera poses must be a 7xN double matrix!" );

  // Test the Calibration Matrix input parameter
  if ( mxGetNumberOfDimensions ( prhs[CALIBRATION] ) != 2 || !mxIsDouble ( prhs[CALIBRATION] ) )
    mexErrMsgTxt ( ERROR_HEADER "Calibration matrix must be a 3x3 double matrix!" );

  // Calibration Matrix must be a 3x3 matrix.
  if ( mxGetM ( prhs[CALIBRATION] ) != 3 || mxGetN ( prhs[CALIBRATION] ) != 3 )
    mexErrMsgTxt ( ERROR_HEADER "Calibration matrix must be a 3x3 double matrix!" );

  // Intrinsic Parameter Matrix
  double *IntPar = (double *)mxGetPr ( prhs[CALIBRATION] );

  double K[9];           // Intrinsic Calibration Parameters
  // The Intrinsic Parameters Matrix
  K[0] = *IntPar++;  K[1] = *IntPar++;  K[2] = *IntPar++;
  K[3] = *IntPar++;  K[4] = *IntPar++;  K[5] = *IntPar++;
  K[6] = *IntPar++;  K[7] = *IntPar++;  K[8] = *IntPar++;

  double ical[5];        // Intrinsic Calibration Parameters for Optimization
  // Read from K transposed!!! Since matlab uses column-wise storage
  ical[0] = K[0];        // fu
  ical[1] = K[6];        // u0
  ical[2] = K[7];        // v0
  ical[3] = K[4] / K[0]; // ar
  ical[4] = K[3];        // s

  bool Verbose, Expert, AnalyticJac;   // Verbose, Expert mode and Analitical Jacobian computation
  sint HowTo;                           // Select the minimization type
  sint MaxIterations;                   // Maximum number of iterations when optimizing
  sint NumConstFrames;                  // Number of constant camera poses (they will not be optimized)

  // Retrieve Verbosity
  if ( mxGetNumberOfElements ( prhs[VERBOSE] ) != 1 || !mxIsLogical ( prhs[VERBOSE] ) )
    mexErrMsgTxt ( ERROR_HEADER "Verbose must be a logical scalar value!" );
  else        
    Verbose = *(bool *)mxGetPr ( prhs[VERBOSE] );

  // Retrieve the Howto
  if ( mxGetNumberOfElements ( prhs[TYPE] ) != 1 || !mxIsInt8 ( prhs[TYPE] ) )
    mexErrMsgTxt ( ERROR_HEADER "Type must be a 8Bit integer scalar value!" );
  else        
    HowTo = static_cast<sint> ( *(unsigned char *)mxGetPr ( prhs[TYPE] ) );

  // Retrieve the expert mode
  if ( mxGetNumberOfElements ( prhs[EXPERT] ) != 1 || !mxIsLogical ( prhs[EXPERT] ) )
    mexErrMsgTxt ( ERROR_HEADER "Expert must be a logical scalar value!" );
  else        
    Expert = *(bool *)mxGetPr ( prhs[EXPERT] );

  // Retrieve the jacobian type
  if ( mxGetNumberOfElements ( prhs[ANALYTICJAC] ) != 1 || !mxIsLogical ( prhs[ANALYTICJAC] ) )
    mexErrMsgTxt ( ERROR_HEADER "AnalyticJac must be a logical scalar value!" );
  else        
    AnalyticJac = *(bool *)mxGetPr ( prhs[ANALYTICJAC] );

  // Retrieve the maximum number of iterations
  if ( mxGetNumberOfElements ( prhs[MAXITERATIONS] ) != 1 || !mxIsDouble ( prhs[MAXITERATIONS] ) )
    mexErrMsgTxt ( ERROR_HEADER "MaxIterations must be a scalar value!" );
  else        
    MaxIterations = static_cast<sint> ( *(double *)mxGetPr ( prhs[MAXITERATIONS] ) );

  // Retrieve the maximum number of constant frames
  if ( mxGetNumberOfElements ( prhs[NUMCONSTFRAMES] ) != 1 || !mxIsDouble ( prhs[NUMCONSTFRAMES] ) )
    mexErrMsgTxt ( ERROR_HEADER "NumConstFrames must be a scalar value!" );
  else        
    NumConstFrames = static_cast<sint> ( *(double *)mxGetPr ( prhs[NUMCONSTFRAMES] ) );

  double opts[SBA_OPTSSZ];  // Options

  // Retrieve the minimization options
  if ( mxGetNumberOfElements ( prhs[INITIALMU] ) != 1 || !mxIsDouble ( prhs[INITIALMU] ) )
    mexErrMsgTxt ( ERROR_HEADER "InitialMu must be a double scalar value!" );
  else        
    opts[0] = *(double *)mxGetPr ( prhs[INITIALMU] );

  if ( mxGetNumberOfElements ( prhs[TERMTHRESH1] ) != 1 || !mxIsDouble ( prhs[TERMTHRESH1] ) )
    mexErrMsgTxt ( ERROR_HEADER "TermThresh1 must be a double scalar value!" );
  else        
    opts[1] = *(double *)mxGetPr ( prhs[TERMTHRESH1] );

  if ( mxGetNumberOfElements ( prhs[TERMTHRESH2] ) != 1 || !mxIsDouble ( prhs[TERMTHRESH2] ) )
    mexErrMsgTxt ( ERROR_HEADER "TermThresh2 must be a double scalar value!" );
  else        
    opts[2] = *(double *)mxGetPr ( prhs[TERMTHRESH2] );

  if ( mxGetNumberOfElements ( prhs[TERMTHRESH3] ) != 1 || !mxIsDouble ( prhs[TERMTHRESH3] ) )
    mexErrMsgTxt ( ERROR_HEADER "TermThresh3 must be a double scalar value!" );
  else        
    opts[3] = *(double *)mxGetPr ( prhs[TERMTHRESH3] );

  if ( mxGetNumberOfElements ( prhs[TERMTHRESH4] ) != 1 || !mxIsDouble ( prhs[TERMTHRESH4] ) )
    mexErrMsgTxt ( ERROR_HEADER "TermThresh4 must be a double scalar value!" );
  else        
    opts[4] = *(double *)mxGetPr ( prhs[TERMTHRESH4] );

  // Number of 3D points
  sint numpts3D = mxGetN ( prhs[POINTS] );

  // Compute the number of 3D points
  if ( mxGetM ( prhs[POINTS] ) != 2  || numpts3D <= 0 || !mxIsCell ( prhs[POINTS] ) )
    mexErrMsgTxt ( ERROR_HEADER "Points must be a cell-array containing on each column:\n"
                   CELL_CONTENT_MSG1 );
  
  mxArray *CurCell;      // Pointer to the current Cell
  sint Rows, Columns;    // Current number of rows and columns
  double *Pd;            // Pointer to a double array
  double *imgpts;        // Store the set of 2D points
  sint nobs;             // Number of observations
  sint i, j, k;

  double *CovImgPts = NULL;  // No covariances provided
  bool FixedCal = true;      // Fixed Calibration

  // Check the cell array content before allocating memory
  checkPoints ( ERROR_HEADER, CELL_CONTENT_MSG1, prhs[POINTS], numpts3D, nframes );

  // Create the motion structure with the camera poses
  double *motstruct;      // The motion structure: (q1 q2 q3 q4 X, Y, Z), (X, Y, Z)
  char *vmask;            // The frame mask: (nframes x numpts3D) to define
                          // in which frames a 3D point is imaged

  double *PtMotstruct, *Ptimgpts;  // To scan data structures

  // Allocate space for camera poses and the set of 3D points (Variables to optimize)
  if ( ( motstruct = new double[nframes*cnp+numpts3D*pnp] ) == NULL )
    mexErrMsgTxt ( ERROR_HEADER "Not enought memory to store camera positions and 3D point structure!" );

  // Pointer to the source data
  double *Cameras = (double *)mxGetPr ( prhs[CAMERAS] );

  // To free memory later
  PtMotstruct = motstruct;

  // Fill the motion structure first with the camera poses
  for ( i = 0; i < nframes; ++i  )
  {
    // Convert input quaternion to qx, qy, qz (normalized)
    quat2vec ( Cameras, inpcnp, PtMotstruct, cnp );

    Cameras += inpcnp;
    PtMotstruct += cnp;
  }
  
  // Compute the number of 2D projections
  sint numprojs = calcNum2DProj ( prhs[POINTS], numpts3D );

  // Allocate for the 2D set of points (Constraints)
  if ( ( imgpts = new double[numprojs*mnp] ) == NULL )
  {
    delete []motstruct;
    mexErrMsgTxt ( ERROR_HEADER "Not enought memory to store the 2D points structure!" );
  }

  // Allocate the 2D mask ( nframes x numpts3D )
  if ( ( vmask = new char[nframes*numpts3D] ) == NULL )
  {
    delete []motstruct;
    delete []imgpts;
    mexErrMsgTxt ( ERROR_HEADER "Not enought memory to store 3D Points vs Camera mask structure!" );
  }
  
  // Reset the mask
  for ( i = 0; i < nframes*numpts3D; i++ )
    vmask[i] = 0;

  // Count number of observations
  nobs = 0;

  // To free memory later
  Ptimgpts = imgpts;
  // Fill the 3D and 2D points.
  for ( i = 0; i < numpts3D; i++ )
  {
    // Read X, Y, Z
    CurCell = mxGetCell ( prhs[POINTS], i * 2 + 0 );
    Rows = mxGetM ( CurCell );
    Columns = mxGetN ( CurCell );

    Pd = (double *)mxGetPr ( CurCell );
    // Copy to the motion structure
    *PtMotstruct++ = *Pd++;
    *PtMotstruct++ = *Pd++;
    *PtMotstruct++ = *Pd++;

    // Read N, x, y
    CurCell = mxGetCell ( prhs[POINTS], i * 2 + 1 );
    Rows = mxGetM ( CurCell );
    Columns = mxGetN ( CurCell );

    // Current Cell Pointer
    Pd = (double *)mxGetPr ( CurCell );

    for ( j = 0; j < Columns; j++ )
    {
      // Get N
      k = static_cast<sint> ( *Pd++ ) - 1;

      // Copy x, y
      *Ptimgpts++ = *Pd++;
      *Ptimgpts++ = *Pd++;

      // Unmask
      vmask[i*nframes+k] = 1;
      nobs++;
    }
  }
   
  double *motstruct_copy;       // Pointer to store a temporal copy used in 2-Step minimization
  double info[SBA_INFOSZ];      // To store minimization returning values
  double phi;                   // Store resulting error in 2-Step minimization
  sint n;                       // Store minimization status
  clock_t start_time, end_time; // To compute the time

  // Number of variables
  sint nvars = 0;

  switch ( HowTo )
  {
    case BA_MOTSTRUCT:
    case BA_MOT_MOTSTRUCT: nvars = nframes * cnp + numpts3D * pnp; break;
    case BA_MOT:           nvars = nframes * cnp; break;
    case BA_STRUCT:        nvars = numpts3D * pnp; break;
    default:
      delete []motstruct;
      delete []imgpts;
//      if ( CovImgPts) delete []CovImgPts;
      delete []vmask;
      mexErrMsgTxt ( ERROR_HEADER "Unknown Bundle Adjustement method!" );
    break;
  }

  // Observations
  nobs *= mnp;

  if ( nobs >= nvars )
  {
    // set up globs structure
    globs.intrcalib = ical;
    globs.nccalib = 0;        // Not used (since using Fixed Calibration)
    globs.pnp = pnp;          // 3D Point number of elements (X, Y, Z)
    globs.cnp = cnp;          // Pose number of elemets (q2, q3, q4, X, Y, Z)
    globs.mnp = mnp;          // 2D Point number of elements (x, y) * 2
    globs.ptparams = NULL;
    globs.camparams = NULL;

    start_time = clock ( );
    switch ( HowTo )
    {
      case BA_MOTSTRUCT: // BA for motion & structure
        if ( Expert )
          n = sba_motstr_levmar_x ( numpts3D, nframes, NumConstFrames, vmask, motstruct, cnp, pnp, imgpts, CovImgPts, mnp,
                                    FixedCal ? img_projsRTS_x : img_projsKRTS_x, AnalyticJac ? ( FixedCal ? img_projsRTS_jac_x : img_projsKRTS_jac_x ) : NULL,
                                    (void *)(&globs), MaxIterations, VerboseBundle, opts, info );
        else
          n = sba_motstr_levmar ( numpts3D, nframes, NumConstFrames, vmask, motstruct, cnp, pnp, imgpts, CovImgPts, mnp,
                                  FixedCal ? img_projRTS : img_projKRTS, AnalyticJac ? ( FixedCal ? img_projRTS_jac : img_projKRTS_jac ) : NULL,
                                  (void *)(&globs), MaxIterations, VerboseBundle, opts, info );
      break;

      case BA_MOT: // BA for motion only
        globs.ptparams = motstruct + nframes * cnp;
        if ( Expert )
          n = sba_mot_levmar_x ( numpts3D, nframes, NumConstFrames, vmask, motstruct, cnp, imgpts, CovImgPts, mnp,
                                 FixedCal ? img_projsRT_x : img_projsKRT_x, AnalyticJac ? ( FixedCal ? img_projsRT_jac_x : img_projsKRT_jac_x ) : NULL,
                                 (void *)(&globs), MaxIterations, VerboseBundle, opts, info );
        else
          n = sba_mot_levmar ( numpts3D, nframes, NumConstFrames, vmask, motstruct, cnp, imgpts, CovImgPts, mnp,
                               FixedCal ? img_projRT : img_projKRT, AnalyticJac ? ( FixedCal ? img_projRT_jac : img_projKRT_jac ) : NULL,
                               (void *)(&globs), MaxIterations, VerboseBundle, opts, info);
      break;

      case BA_STRUCT: // BA for structure only
        globs.camparams = motstruct;
        if ( Expert )
          n = sba_str_levmar_x ( numpts3D, nframes, vmask, motstruct + nframes * cnp, pnp, imgpts, CovImgPts, mnp,
                                 FixedCal ? img_projsS_x : img_projsKS_x, AnalyticJac ? ( FixedCal ? img_projsS_jac_x : img_projsKS_jac_x ) : NULL,
                                 (void *)(&globs), MaxIterations, VerboseBundle, opts, info );
        else
          n = sba_str_levmar ( numpts3D, nframes, vmask, motstruct + nframes * cnp, pnp, imgpts, CovImgPts, mnp,
                               FixedCal ? img_projS : img_projKS, AnalyticJac ? ( FixedCal ? img_projS_jac : img_projKS_jac ) : NULL,
                               (void *)(&globs), MaxIterations, VerboseBundle, opts, info);
      break;

      case BA_MOT_MOTSTRUCT: // BA for motion only; if error too large, then BA for motion & structure
        if ( ( motstruct_copy = new double[nvars] ) == NULL )
        {
          delete []motstruct;
          delete []imgpts;
//          if ( CovImgPts) delete []CovImgPts;
          delete []vmask;
          mexErrMsgTxt ( ERROR_HEADER "Memory allocation to copy motstruct failed!" );
        }

        memcpy ( motstruct_copy, motstruct, nvars * sizeof ( double ) ); // Save starting point for later use
        globs.ptparams = motstruct + nframes * cnp;
//        nvars = nframes * cnp;
        if ( Expert )
          n = sba_mot_levmar_x ( numpts3D, nframes, NumConstFrames, vmask, motstruct, cnp, imgpts, CovImgPts, mnp,
                                 FixedCal ? img_projsRT_x : img_projsKRT_x, AnalyticJac ? ( FixedCal ? img_projsRT_jac_x : img_projsKRT_jac_x ) : NULL,
                                 (void *)(&globs), MaxIterations, VerboseBundle, opts, info );
        else
          n = sba_mot_levmar ( numpts3D, nframes, NumConstFrames, vmask, motstruct, cnp, imgpts, CovImgPts, mnp,
                               FixedCal ? img_projRT : img_projKRT, AnalyticJac ? ( FixedCal ? img_projRT_jac : img_projKRT_jac ) : NULL,
                               (void *)(&globs), MaxIterations, VerboseBundle, opts, info );

        if ( ( phi = info[1] / numprojs ) > MSBA_MAX_REPROJ_ERROR )
        {
          printfmexWarnMsgTxt ( ERROR_HEADER "Refining structure (motion only error %g)...\n", phi );

          memcpy ( motstruct, motstruct_copy, nvars * sizeof ( double ) ); // Reset starting point
          if ( Expert )
            n = sba_motstr_levmar_x ( numpts3D, nframes, NumConstFrames, vmask, motstruct, cnp, pnp, imgpts, NULL, mnp,
                                      FixedCal ? img_projsRTS_x : img_projsKRTS_x, AnalyticJac ? ( FixedCal ? img_projsRTS_jac_x : img_projsKRTS_jac_x ) : NULL,
                                      (void *)(&globs), MaxIterations, VerboseBundle, opts, info );
          else
            n = sba_motstr_levmar ( numpts3D, nframes, NumConstFrames, vmask, motstruct, cnp, pnp, imgpts, NULL, mnp,
                                    FixedCal ? img_projRTS : img_projKRTS, AnalyticJac ? ( FixedCal ? img_projRTS_jac : img_projKRTS_jac ) : NULL,
                                    (void *)(&globs), MaxIterations, VerboseBundle, opts, info );
        }
        else
          printfmexWarnMsgTxt ( ERROR_HEADER "Only refined motion. Motion+structure was not done because: (phi = info[1] / numprojs = %g  >  SBA_MAX_REPROJ_ERROR = %g, info[1] = %g, numprojs = %d)...\n",
                                phi, SBA_MAX_REPROJ_ERROR, info[1], numprojs );

        delete []motstruct_copy;
      break;

      default:
        mexErrMsgTxt ( ERROR_HEADER "Unknown Bundle Adjustement method!" );
      break;
    }
    end_time = clock ( );

    if ( Verbose )
    {
      static const char *HowToName[] = { "BA_MOTSTRUCT", "BA_MOT", "BA_STRUCT", "BA_MOT_MOTSTRUCT" };
      static const char *TerminationReason[] = { "Unknown",
                                                 "Small gradient J^T e", 
                                                 "Small dp",
                                                 "Reached maximum number of iterations",
                                                 "Small relative reduction in ||e||_2",
                                                 "Small ||e||_2",
                                                 "Too many attempts to increase damping. Restart with increased mu!",
                                                 "Invalid (i.e. NaN or Inf) \"func\" values. This is a user error!" };
                                
      std::cerr << "SBA (Mono) using: " << numpts3D << " 3D points, " << nframes << " frames, " << numprojs << " image projections (" << nvars << " variables)" << std::endl;

      std::cerr << "  Method: " << HowToName[HowTo] << ", " << ( Expert ? "expert" : "simple" ) << " driver, " << ( AnalyticJac ? "analytic" : "approximate" ) << " Jacobian, "
                << ( FixedCal ? "fixed" : "variable" ) << " intrinsics, " << ( CovImgPts ? "with" : "without" ) << " covariances";

      if ( globs.nccalib ) std::cerr << " (" << globs.nccalib << " fixed calibration params)";
      std::cerr << std::endl;
                
      std::cerr << "  Returned: " << n << " (" << ( n < 0 ? "SBA_ERROR" : "Iteration count" ) << ") after " << info[5] << " iterations, " << (int)info[7] << "/" << (int)info[8]
                                  << " func/fjac avaluations of " << (int)info[9] << " linear systems" << std::endl;
      std::cerr << "  Termination reason: " << info[6] << " (" << ( ((int)info[6] >= 1 && (int)info[6] <= 7) ? TerminationReason[(int)info[6]] : TerminationReason[0] )
                                 << "). Error " << info[1]/numprojs << " [Initial " << info[0]/numprojs << "]" << std::endl;
      double Secs = ((double) (end_time - start_time)) / CLOCKS_PER_SEC;
      std::cerr << "  Elapsed time: ";
      if ( Secs >= 1 )
        std::cerr << Secs << " sec";
      else
        std::cerr << Secs * 1000 << " msec";
      std::cerr << std::endl;
    }
  }
  else
    printfmexWarnMsgTxt ( ERROR_HEADER "Cannot solve a problem with fewer measurements [%d] than unknowns [%d]!\n", nobs, nvars );

  // To free structure later
  PtMotstruct = motstruct;

  // Copy to the resulting matrices
  plhs[CAMERASR] = mxCreateDoubleMatrix ( inpcnp, nframes, mxREAL );
  Pd = (double *)mxGetPr ( plhs[CAMERASR] );
  for ( i = 0; i < nframes; ++i )
  {
    // Convert normalized quaternion qx, qy, qz to quaternion (qr, qx, qy, qz)    
    vec2quat ( PtMotstruct, cnp, Pd, inpcnp );

    Pd += inpcnp;
    PtMotstruct += cnp;
  }

  plhs[POINTSR] = mxCreateDoubleMatrix ( pnp, numpts3D, mxREAL );
  Pd = (double *)mxGetPr ( plhs[POINTSR] );
  for ( i = 0; i < numpts3D*pnp; i++ )
    *Pd++ = *PtMotstruct++;

  plhs[REPRERROR] = mxCreateDoubleMatrix ( 2, 1, mxREAL );
  Pd = (double *)mxGetPr ( plhs[REPRERROR] );
  *Pd++ = info[0] / numprojs;
  *Pd = info[1] / numprojs;

  // Release memory
  delete []motstruct;
  delete []imgpts;
//  if ( CovImgPts) delete []CovImgPts;
  delete []vmask;

  return;
}
