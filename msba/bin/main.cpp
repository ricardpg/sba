/* Euclidean bundle adjustment demo using the sba package */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include <sba.h>        // Sparse Bundle Adjustement
#include <sba_types.h>

#ifdef __cplusplus
extern "C" {
#endif
  #include <eucsbademo.h>
  #include <readparams.h>
#ifdef __cplusplus
}
#endif

#include "fileio.h"
#include "quat.h"
#include "msba.h"       // Definitions

#define CLOCKS_PER_MSEC (CLOCKS_PER_SEC/1000.0)

/* Driver for sba_xxx_levmar */
void msba_driver ( char *camsfname, char *ptsfname, char *calibfname, char *finalcamera, char *final3dpoints, char *strnconstframes, char *strmaxiterations, sint howto )
{
  double *motstruct, *motstruct_copy, *imgpts, *covimgpts;
  double K[9], ical[5]; // intrinsic calibration matrix & temp. storage for its params
  char *vmask;
  double opts[SBA_OPTSSZ], info[SBA_INFOSZ], phi;
//  sint howto;
  sint expert, analyticjac, fixedcal, n, prnt, verbose=0;
  sint nframes, numpts3D, numprojs, nvars;
//  const sint nconstframes=1;
  const sint cnp=6, /* 3 rot params + 3 trans params */
            pnp=3, /* euclidean 3D points */
            mnp=2; /* image points are 2D */
  const sint filecnp = cnp+1;

  static char *howtoname[]={"BA_MOTSTRUCT", "BA_MOT", "BA_STRUCT", "BA_MOT_MOTSTRUCT"};

  clock_t start_time, end_time;

  sint nconstframes = atoi ( strnconstframes );
  sint MaxIterations = atoi ( strmaxiterations );

  /* NOTE: readInitialSBAEstimate() sets covimgpts to NULL if no covariances are supplied */
  readInitialSBAEstimate(camsfname, ptsfname, cnp, pnp, mnp, quat2vec, filecnp, //NULL, 0,
                         &nframes, &numpts3D, &numprojs, &motstruct, &imgpts, &covimgpts, &vmask);

  //printSBAData(motstruct, cnp, pnp, mnp, vec2quat, filecnp, nframes, numpts3D, imgpts, numprojs, vmask);

  /* set up globs structure */
  globs.cnp=cnp; globs.pnp=pnp; globs.mnp=mnp;
  if(calibfname){ // read intrinsics only if fixed for all cameras
    readCalibParams(calibfname, K);
    ical[0]=K[0]; // fu
    ical[1]=K[2]; // u0
    ical[2]=K[5]; // v0
    ical[3]=K[4]/K[0]; // ar
    ical[4]=K[1]; // s
    globs.intrcalib=ical;
    fixedcal=1; /* fixed intrinsics */
  }
  else{ // intrinsics are to be found in the cameras parameter file
    globs.intrcalib=NULL;
    /* specify the number of intrinsic parameters that are to be fixed
     * equal to their initial values, as follows:
     * 0: all free, 1: skew fixed, 2: skew, ar fixed, 4: skew, ar, ppt fixed
     * Note that a value of 3 does not make sense
     */
    globs.nccalib=1;
    fixedcal=0; /* varying intrinsics */
  }

  globs.ptparams=NULL;
  globs.camparams=NULL;

  /* call sparse LM routine */
  opts[0]=SBA_INIT_MU; opts[1]=SBA_STOP_THRESH; opts[2]=SBA_STOP_THRESH;
  opts[3]=SBA_STOP_THRESH;
  //opts[3]=0.05*numprojs; // uncomment to force termination if the average reprojection error drops below 0.05
  opts[4]=0.0;
  //opts[4]=1E-05; // uncomment to force termination if the relative reduction in the RMS reprojection error drops below 1E-05

  /* Notice the various BA options demonstrated below */

  /* minimize motion & structure, motion only, or
   * motion and possibly motion & structure in a 2nd pass?
   */
  //howto=BA_MOTSTRUCT;
  //howto=BA_MOT;
  //howto=BA_STRUCT;
  //howto=BA_MOT_MOTSTRUCT;

  /* simple or expert drivers? */
  //expert=0;
  expert=1;

  /* analytic or approximate jacobian? */
  //analyticjac=0;
  analyticjac=1;

  /* print motion & structure estimates,
   * motion only or structure only upon completion?
   */
  prnt=BA_NONE;
  //prnt=BA_MOTSTRUCT;
  //prnt=BA_MOT;
  //prnt=BA_STRUCT;

  start_time=clock();
  switch(howto){
    case BA_MOTSTRUCT: /* BA for motion & structure */
      nvars=nframes*cnp+numpts3D*pnp;
      if(expert)
        n=sba_motstr_levmar_x(numpts3D, nframes, nconstframes, vmask, motstruct, cnp, pnp, imgpts, covimgpts, mnp,
                            fixedcal? img_projsRTS_x : img_projsKRTS_x, analyticjac? (fixedcal? img_projsRTS_jac_x : img_projsKRTS_jac_x) : NULL,
                            (void *)(&globs), MaxIterations, verbose, opts, info);
      else
        n=sba_motstr_levmar(numpts3D, nframes, nconstframes, vmask, motstruct, cnp, pnp, imgpts, covimgpts, mnp,
                            fixedcal? img_projRTS : img_projKRTS, analyticjac? (fixedcal? img_projRTS_jac : img_projKRTS_jac) : NULL,
                            (void *)(&globs), MaxIterations, verbose, opts, info);
    break;

    case BA_MOT: /* BA for motion only */
      globs.ptparams=motstruct+nframes*cnp;
      nvars=nframes*cnp;
      if(expert)
        n=sba_mot_levmar_x(numpts3D, nframes, nconstframes, vmask, motstruct, cnp, imgpts, covimgpts, mnp,
                          fixedcal? img_projsRT_x : img_projsKRT_x, analyticjac? (fixedcal? img_projsRT_jac_x : img_projsKRT_jac_x) : NULL,
                          (void *)(&globs), MaxIterations, verbose, opts, info);
      else
        n=sba_mot_levmar(numpts3D, nframes, nconstframes, vmask, motstruct, cnp, imgpts, covimgpts, mnp,
                          fixedcal? img_projRT : img_projKRT, analyticjac? (fixedcal? img_projRT_jac : img_projKRT_jac) : NULL,
                          (void *)(&globs), MaxIterations, verbose, opts, info);
    break;

    case BA_STRUCT: /* BA for structure only */
      globs.camparams=motstruct;
      nvars=numpts3D*pnp;
      if(expert)
        n=sba_str_levmar_x(numpts3D, nframes, vmask, motstruct+nframes*cnp, pnp, imgpts, covimgpts, mnp,
                          fixedcal? img_projsS_x : img_projsKS_x, analyticjac? (fixedcal? img_projsS_jac_x : img_projsKS_jac_x) : NULL,
                          (void *)(&globs), MaxIterations, verbose, opts, info);
      else
        n=sba_str_levmar(numpts3D, nframes, vmask, motstruct+nframes*cnp, pnp, imgpts, covimgpts, mnp,
                          fixedcal? img_projS : img_projKS, analyticjac? (fixedcal? img_projS_jac : img_projKS_jac) : NULL,
                          (void *)(&globs), MaxIterations, verbose, opts, info);
    break;

    case BA_MOT_MOTSTRUCT: /* BA for motion only; if error too large, then BA for motion & structure */
      if((motstruct_copy=(double *)malloc((nframes*cnp + numpts3D*pnp)*sizeof(double)))==NULL){
        fprintf(stderr, "memory allocation failed in sba_driver()\n");
        exit(1);
      }

      memcpy(motstruct_copy, motstruct, (nframes*cnp + numpts3D*pnp)*sizeof(double)); // save starting point for later use
      globs.ptparams=motstruct+nframes*cnp;
      nvars=nframes*cnp;

      if(expert)
        n=sba_mot_levmar_x(numpts3D, nframes, nconstframes, vmask, motstruct, cnp, imgpts, covimgpts, mnp,
                          fixedcal? img_projsRT_x : img_projsKRT_x, analyticjac? (fixedcal? img_projsRT_jac_x : img_projsKRT_jac_x) : NULL,
                          (void *)(&globs), MaxIterations, verbose, opts, info);
      else
        n=sba_mot_levmar(numpts3D, nframes, nconstframes, vmask, motstruct, cnp, imgpts, covimgpts, mnp,
                        fixedcal? img_projRT : img_projKRT, analyticjac? (fixedcal? img_projRT_jac : img_projKRT_jac) : NULL,
                        (void *)(&globs), MaxIterations, verbose, opts, info);

      if((phi=info[1]/numprojs)>SBA_MAX_REPROJ_ERROR){
        fflush(stdout); fprintf(stdout, "Refining structure (motion only error %g)...\n", phi); fflush(stdout);
        memcpy(motstruct, motstruct_copy, (nframes*cnp + numpts3D*pnp)*sizeof(double)); // reset starting point

        if(expert)
          n=sba_motstr_levmar_x(numpts3D, nframes, nconstframes, vmask, motstruct, cnp, pnp, imgpts, NULL, mnp,
                                fixedcal? img_projsRTS_x : img_projsKRTS_x, analyticjac? (fixedcal? img_projsRTS_jac_x : img_projsKRTS_jac_x) : NULL,
                                (void *)(&globs), MaxIterations, verbose, opts, info);
        else
          n=sba_motstr_levmar(numpts3D, nframes, nconstframes, vmask, motstruct, cnp, pnp, imgpts, NULL, mnp,
                              fixedcal? img_projRTS : img_projKRTS, analyticjac? (fixedcal? img_projRTS_jac : img_projKRTS_jac) : NULL,
                              (void *)(&globs), MaxIterations, verbose, opts, info);
      }
      free(motstruct_copy);
    break;

    default:
      fprintf(stderr, "unknown BA method \"%ld\" in sba_driver()!\n", howto);
      exit(1);
  }
  end_time=clock();


  fflush(stdout);
  fprintf(stdout, "SBA using %ld 3D pts, %ld frames and %ld image projections, %ld variables\n", numpts3D, nframes, numprojs, nvars);
  fprintf(stdout, "\nMethod %s, %s driver, %s Jacobian, %s intrinsics, %s covariances", howtoname[howto],
                  expert? "expert" : "simple",
                  analyticjac? "analytic" : "approximate",
                  fixedcal? "fixed" : "variable",
                  covimgpts? "with" : "without");
  if(globs.nccalib) fprintf(stdout, " (%ld fixed)", globs.nccalib);
  fputs("\n\n", stdout);
  fprintf(stdout, "SBA returned %ld in %g iter, reason %g, error %g [initial %g], %ld/%ld func/fjac evals, %ld lin. systems\n", n,
                    info[5], info[6], info[1]/numprojs, info[0]/numprojs, (sint)info[7], (sint)info[8], (sint)info[9]);
  fprintf(stdout, "Elapsed time: %.2g seconds, %.2g msecs\n", ((double) (end_time - start_time)) / CLOCKS_PER_SEC,
                  ((double) (end_time - start_time)) / CLOCKS_PER_MSEC);
  fflush(stdout);

  /* refined motion and structure are now in motstruct */
  switch(prnt){
    case BA_NONE:
    break;

    case BA_MOTSTRUCT:
      printSBAMotionData(motstruct, nframes, cnp, vec2quat, filecnp);
      printSBAStructureData(motstruct, nframes, numpts3D, cnp, pnp);
    break;

    case BA_MOT:
      printSBAMotionData(motstruct, nframes, cnp, vec2quat, filecnp);
    break;

    case BA_STRUCT:
      printSBAStructureData(motstruct, nframes, numpts3D, cnp, pnp);
    break;

    default:
      fprintf(stderr, "unknown print option \"%ld\" in sba_driver()!\n", prnt);
      exit(1);
  }

  /* refined motion and structure are now in motstruct */
  printSBAData ( motstruct, cnp, pnp, mnp, vec2quat, filecnp, nframes, numpts3D, imgpts, numprojs, vmask );

  saveResults ( finalcamera, final3dpoints, nframes, numpts3D, motstruct );

  /* just in case... */
  globs.intrcalib=NULL;
  globs.nccalib=0;

  free(motstruct);
  free(imgpts);
  if(covimgpts) free(covimgpts);
  free(vmask);
}

int main(int argc, char *argv[])
{
  sint Type;

  if ( argc != 8 && argc != 9 )
  {
    fprintf(stderr, "Usage:\n%s <Cameras Filename> <Points Filename> <Intrinsic Calibration Filename> <Final Cameras Filename> <Final 3D Filename> <Num. Const Frames> <Max. Iterations> [Type]\n", argv[0]);
    fprintf(stderr, "\nType must be: BA_MOTSTRUCT (default), BA_MOT, BA_STRUCT, BA_MOT_MOTSTRUCT\n" );

    exit ( -1 );
  }

  if ( argc == 8 )
    Type = BA_MOTSTRUCT;
  else
  {
    if ( !strcmp ( argv[8], "BA_MOTSTRUCT" ) ) Type = BA_MOTSTRUCT;
    if ( !strcmp ( argv[8], "BA_MOT" ) ) Type = BA_MOT;
    if ( !strcmp ( argv[8], "BA_STRUCT" ) ) Type = BA_STRUCT;
    if ( !strcmp ( argv[8], "BA_MOT_MOTSTRUCT" ) ) Type = BA_MOT_MOTSTRUCT;
  }

  msba_driver(argv[1], argv[2], argv[3], argv[4], argv[5], argv[6], argv[7], Type );

  return 0;
}

