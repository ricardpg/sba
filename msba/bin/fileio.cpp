#include <stdio.h>
#include <stdlib.h>

#include "msba.h"
#include "fileio.h"

bool saveResults ( const char *finalcamera, const char *final3dpoints, const int NumFrames, const int Num3DPoints, double *motstruct )
{
  FILE *Camera;
  FILE *Points;
  int i;
 
  if ( ( Camera = fopen ( finalcamera, "w" ) ) < 0 )
  {
     fprintf ( stderr, "File '%s' to write final cameras cannot be opened for writting!", finalcamera );
     return false;
  }

  if ( ( Points = fopen ( final3dpoints, "w" ) ) < 0 )
  {
     fclose ( Camera );
     fprintf ( stderr, "File '%s' to write final 3d points cannot be opened for writting!", finalcamera );
     return false;
  }

  // To convert back vector to quaternion
  double NOut[4+3];

  for ( i = 0; i < NumFrames; i++, motstruct += 6 )
  {
    vec2quat ( motstruct, 3+3, NOut, 4+3 );
    fprintf ( Camera, "%.16g\t%.16g\t%.16g\t%.16g\t%.16g\t%.16g\t%.16g\n",
                      NOut[0], NOut[1], NOut[2],
                      NOut[3], NOut[4], NOut[5], NOut[6] );
  }

  fclose ( Camera );

  for ( i = 0; i < Num3DPoints; i++, motstruct += 3 )
    fprintf ( Points, "%.16g\t%.16g\t%.16g\n", motstruct[0], motstruct[1], motstruct[2] );

  fclose ( Points );
  
  return true;
}
