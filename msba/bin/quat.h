#ifndef __QUAT_H__
#define __QUAT_H__

#include <sba_types.h>

void quat2vec(double *inp, sint nin, double *outp, sint nout);
void vec2quat(double *inp, sint nin, double *outp, sint nout);

#endif
