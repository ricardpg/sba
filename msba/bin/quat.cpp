#include <cmath>

#include <sba_types.h>

#include "quat.h"

/* convert a vector of camera parameters so that rotation is represented by
 * the vector part of the input quaternion. The function converts the
 * input quaternion into a unit one with a positive scalar part. Remaining
 * parameters are left unchanged.
 *
 * Input parameter layout: intrinsics (5, optional), rot. quaternion (4), translation (3)
 * Output parameter layout: intrinsics (5, optional), rot. quaternion vector part (3), translation (3)
 */
void quat2vec(double *inp, sint nin, double *outp, sint nout)
{
double mag, sg;
register sint i;

  /* intrinsics */
  if(nin==5+7) // are they present?
    for(i=0; i<5; ++i)
      outp[i]=inp[i];
  else
    i=0;

  /* rotation */
  /* normalize and ensure that the quaternion's scalar component is positive; if not,
   * negate the quaternion since two quaternions q and -q represent the same
   * rotation
   */
  mag=sqrt(inp[i]*inp[i] + inp[i+1]*inp[i+1] + inp[i+2]*inp[i+2] + inp[i+3]*inp[i+3]);
  sg=(inp[i]>=0.0)? 1.0 : -1.0;
  mag=sg/mag;
  outp[i]  =inp[i+1]*mag;
  outp[i+1]=inp[i+2]*mag;
  outp[i+2]=inp[i+3]*mag;
  i+=3;

  /* translation*/
  for( ; i<nout; ++i)
    outp[i]=inp[i+1];
}

/* convert a vector of camera parameters so that rotation is represented by
 * a full unit quaternion instead of its input 3-vector part. Remaining
 * parameters are left unchanged.
 *
 * Input parameter layout: intrinsics (5, optional), rot. quaternion vector part (3), translation (3)
 * Output parameter layout: intrinsics (5, optional), rot. quaternion (4), translation (3)
 */
void vec2quat(double *inp, sint nin, double *outp, sint nout)
{
double w;
register sint i;

  /* intrinsics */
  if(nin==5+7-1) // are they present?
    for(i=0; i<5; ++i)
      outp[i]=inp[i];
  else
    i=0;

  /* rotation */
  /* recover the quaternion's scalar component from the vector one */
  w=sqrt(1.0 - (inp[i]*inp[i] + inp[i+1]*inp[i+1] + inp[i+2]*inp[i+2]));
  outp[i]  =w;
  outp[i+1]=inp[i];
  outp[i+2]=inp[i+1];
  outp[i+3]=inp[i+2];
  i+=4;

  /* translation */
  for( ; i<nout; ++i)
    outp[i]=inp[i-1];
}
