%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Monocular Sparse Bundle Adjustement Wrapper for Matlab.
%
%  File          : msba.m
%  Date          : 17/01/2007 - 09/06/2009
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2009 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  msba Sparse Bundle Adjustement to refine the motion and the structure of
%       a set of 3D points.
%
%      [CamerasR PointsR ReprError] = msba ( Calibration, Motions, Points[, SbaParameters] )
%
%     Input Parameters:
%      Calibration: 3x3 Intrinsic parameters camera matrix.
%      Cameras: 7xN camera positions expressed as one Quaterion for the
%               rotation and X, Y, Z for position
%      Points: 2xM Cell array containing the 3D point and the imaged
%              2D points in each row. The cells for the 3D points (1st row)
%              must be a double vector containing (X, Y, Z). The cells for
%              the 2D points (2nd row) must be a 3xK matrix containing at
%              each row the N, x, y values. N is the frame number in which
%              the 3D point is imaged and (x, y) are the 2D coordinates for
%              this point in this frame.
%      SbaParameters: See sbaCheckParameters.m
%      SbaExePath: String containing the path to the Stereo Bundle
%                  Adjustement binary in the file system.
%      TmpPath: System writable folder to store temporary files.
%
%     Output Parameters:
%      CamerasR: 7xN refined camera motions.
%      PointsR: 3xM refined set of 3D points.
%      ReprError: 2x1 vector containing the initial and final reprojection
%                 error respectively.
%
%

function [CamerasR PointsR ReprError] = msba ( Calibration, Cameras, Points, SbaParameters, ...
                                               SbaExePath, TmpPath  )
  % Test the input parameters
  error ( nargchk ( 5, 6, nargin ) );
  error ( nargoutchk ( 2, 3, nargout ) );

  % If don't provided, use default ones
  if nargin == 5; SbaParameters = 1; end

  % Check sba input parameters
  SbaParameters = sbaCheckParameters ( SbaParameters );

  [Kr, Kc] = size ( Calibration );
  if Kr ~= 3 || Kc ~= 3;
    error ( 'MATLAB:msba:Input', 'Calibration must be a 3x3 double matrix!' );
  end

  [cnp, nframes] = size ( Cameras );
  if cnp ~= 7 || nframes <= 0;
    error ( 'MATLAB:msba:Input', 'Cameras must be a 7xN double matrix containing (q1, q2, q3, q4, X, Y, Z) for each camera!' );
  end

  % Check the Points Structure Content
  sbaCheckPoints ( Points, false, false );
  [Cr, numpts3D] = size ( Points );

  if ~ischar ( SbaExePath ) || ~ischar ( TmpPath );
    error ( 'MATLAB:msba:Input', 'SbaExePath must be a system executable and TmpPath must be a system writtable folder!' );
  end

  a = 65;  % 'A'
  b = 90;  % 'Z'
  r = a + ( b - a ) .* rand ( 1, 6 );
  RandomStr = char(r);

  % Filenames corresponding to the input data
  CalibrationFileName = [TmpPath 'msbaCalibration' RandomStr '.txt'];
  CamerasFileName = [TmpPath 'msbaCameras' RandomStr '.txt'];
  PointsFileName = [TmpPath 'msbaPoints' RandomStr '.txt'];

  % Write Input Data to Files
  msbaWrite ( Calibration, Cameras, Points, ...
              CalibrationFileName, CamerasFileName, PointsFileName );

  % Filenames corresponding to the output data
  PointsRFileName = [TmpPath 'msbaPointsR' RandomStr '.txt'];
  CamerasRFileName = [TmpPath 'msbaCamerasR' RandomStr '.txt'];
  DebugRFileName = [TmpPath 'msbaDebugR' RandomStr '.txt'];

  % Use allways the Right Type
  if isstruct ( SbaParameters ); SbaType = SbaParameters.Type; else SbaType = 0; end;
  % Convert to string
  switch ( SbaType )
      case 0,          
        SbaType = ' BA_MOTSTRUCT';
      case 1,
        SbaType = ' BA_MOT';
      case 2,
        SbaType = ' BA_STRUCT';
      case 3,
        SbaType = ' BA_MOT_MOTSTRUCT';
      otherwise,
        SbaType = ' BA_MOTSTRUCT';
  end

  % Execute the command
  SysCommand = [ SbaExePath ' ' CamerasFileName ' ' PointsFileName ' ' CalibrationFileName ' ' CamerasRFileName ' ' PointsRFileName ' ' num2str(SbaParameters.NumConstFrames) ' ' num2str(SbaParameters.MaxIterations) ' ' SbaType ];
  [Status, Str] = system ( SysCommand );

  % Must be taken from the output (Str):
  %   "SBA returned 23 in 23 iter, reason 2, error 0.176473 [initial 2.14707], 29/23 func/fjac evals, 29 lin. systems"

  % Initial, Mid and Final Separators
  ISep = 'error';
  MSep = '[initial ';
  FSep = ']';
  % Positions in the string
  ISepPos = strfind ( Str, ISep );
  MSepPos = strfind ( Str, MSep );
  FSepPos = strfind ( Str, FSep );
  
  ReprError = nan ( 2, 1 );
  % Initial Reprojection
  ReprError(2) = str2double ( Str(ISepPos+size(ISep,2):MSepPos-1) );
  % Final Reprojection
  ReprError(1) = str2double ( Str(MSepPos+size(MSep,2):FSepPos-1) );

  % Save output info
  DebugFID = fopen ( DebugRFileName, 'w' );
  if DebugFID ~= -1;
    fprintf ( DebugFID, '%s', Str );
    fclose ( DebugFID );
  else
    warning ( 'MATLAB:msba:Input', [ 'Debug information couldn''t be written into DebugRFileName="' DebugRFileName '"!' ] );
    disp ( Str );      
  end

  if Status == 0;
    % Read the resulting files
    CamerasR = load ( CamerasRFileName );
    PointsR = load ( PointsRFileName );

    % Delete Temporary Files
    delete ( CamerasRFileName );
    delete ( PointsRFileName );

    % Transpose to be in rows
    CamerasR = CamerasR';
    PointsR = PointsR';
    if ~isempty ( PointsR ) && ~isempty ( CamerasR );
      % Test the Point matrix.
      [ Rows, Columns ] = size ( PointsR );
      if Rows ~= 3 && Columns ~= numpts3D;
        PointsR = [];
        warning ( 'MATLAB:msba:Input', [ 'Resulting 3D points read from "' PointsRFileName '" are not correct!' ] );
      end

      % Test the camera poses matrix.
      [ Rows, Columns ] = size ( CamerasR );
      if Rows ~= 7 && Columns ~= nframes;
        CamerasR = [];
        warning ( 'MATLAB:msba:Input', [ 'Resulting camera poses read from "' CamerasRFileName '" are not correct!' ] );
      end
    end
  else
    disp ( Str );
    error ( 'MATLAB:msba:Input', [ 'SbaExePath="' SbaExePath '" returned an error while executing!' ] );
  end

  % Delete Temporary Files
  delete ( CalibrationFileName );
  delete ( CamerasFileName );
  delete ( PointsFileName );
end
