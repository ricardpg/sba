/*
  Autor(s)      : Jordi Ferrer Plana
  e-mail        : jferrerp@eia.udg.edu
  Branch        : Computer Vision

  Working Group : Underwater Vision Lab
  Project       : -

  Homepage      : http://porcsenglar.udg.edu

  Module        : Checking parameters.

  File          : msbaChecks.cpp
  Date          : 11/10/2007 - 20/04/2009

  Compiler      : MATLAB >= 7.0 & g++ >= 4.x
  Libraries     : -

  Notes         : - File written using ISO-8859-1 encoding.

 -----------------------------------------------------------------------------

  Copyright (C) 2005-2007 by Jordi Ferrer Plana

  This source code is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This source code is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
  more details.

 -----------------------------------------------------------------------------
*/

#include <mex.h>            // Matlab Mex Functions

#include <sba_types.h>

#include "errorMx.h"        // To Print out Errors
#include "msbaChecks.h"     // Definition

void checkPoints ( const char *ErrorHeader, const char *CellContentMsg,
                   const mxArray *Set, const sint Num3DPoints, const sint MaxFrames,
                   const bool IsStereo )
{
  mxArray *CurCell;      // Pointer to the current cell
  double *Ps;            // Pointer to ( N, x, y ) structure
  sint Rows, Columns;    // Number of Rows and Columns for the current cell
  sint i, j, k;          // Iterations
  
  // Number of projections for each 3D point (depends on monocular or stereo)
  sint Num2D = IsStereo ? 4 : 2;

  // Scan all the positions in the cell array
  for ( j = 0; j < Num3DPoints; j++ )
    for ( i = 0; i < 2; i++ )
    {
      // Test the current cell content
      if ( ( CurCell = mxGetCell ( Set, j * 2 + i ) ) == NULL )
        printfmexErrMsgTxt ( "%sThe cell (%d, %d) of Position cell-array must contain:\n%s",
                             ErrorHeader, i + 1, j + 1, CellContentMsg );

      // Compute the number of rows and columns for the current cell
      Rows = mxGetM ( CurCell );
      Columns = mxGetN ( CurCell );

      // Perform differnt checks depending on the row
      switch ( i )
      {
        case 0:  // X, Y, Z
          // Check for Row or Column 3 component vector
          if ( !( Rows == 1 && Columns == 3 ) && ! ( Rows == 3 && Columns == 1 ) )
            printfmexErrMsgTxt ( "%sThe cell (%d, %d) of Position cell-array must be a "
                                 "vector containing (X, Y, Z)!", ErrorHeader, i + 1, j + 1 );
          break;

        case 1:  // [ N, x, y ]'
          if ( Rows != ( 1 + Num2D ) || Columns <= 0 )
            printfmexErrMsgTxt ( "%sThe cell (%d, %d) of Position cell-array must be "
                                 "a matrix containing (Frame Number, x_2D, y_2D) at each column!",
                                 ErrorHeader, i + 1, j + 1 );
            
          // Get the pointer to the current cell content
          Ps = (double *)mxGetPr ( CurCell );
          for ( k = 0; k < Columns; k++ )
          {
            sint CurNumFrames = static_cast<sint> ( *Ps++ );
            // Test N to be less than the number of frames.
            if ( CurNumFrames > MaxFrames )
              printfmexErrMsgTxt ( "%sNumber of frames (%d) in column %d of the cell (%d, %d)\n "
                                   "in the Position cell-array must be less than the maximum frames (%d)!",
                                   ErrorHeader, CurNumFrames, k + 1, i + 1, j + 1, MaxFrames );

            // Skip x, y
            Ps++;
            Ps++;
            // Skip x, y in the case of stereo
            if ( IsStereo ) { Ps++; Ps++; }
          }
          break;

        default: // Cannot happen
          break;
      }
    }
}


sint calcNum2DProj ( const mxArray *Set, const sint Num3DPoints )
{
  mxArray *CurCell;  // Pointer to the current Cell
  sint N;            // Number of frames
  sint i;

  N = 0;
  for ( i = 0; i < Num3DPoints; i++ )
  {
    // Cell for N, x, y  (is at offset 1)
    CurCell = mxGetCell ( Set, i * 2 + 1 );

    // Accumulate number of columns (number of 2D points)
    N += mxGetN ( CurCell );
  }

  return N;
}
