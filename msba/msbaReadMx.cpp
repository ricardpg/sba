/*
  Autor(s)      : Jordi Ferrer Plana
  e-mail        : jferrerp@eia.udg.edu
  Branch        : Computer Vision

  Working Group : Underwater Vision Lab
  Project       : -

  Homepage      : http://porcsenglar.udg.edu

  Module        : Monocular Sparse Bundle Adjustement reading data.

  File          : msbaReadMx.cpp
  Date          : 19/01/2007 - 21/02/2009

  Compiler      : MATLAB >= 7.0 & g++ >= 4.x
  Libraries     : -

  Notes         : - File written using ISO-8859-1 encoding.

 -----------------------------------------------------------------------------

  Copyright (C) 2005-2007 by Jordi Ferrer Plana

  This source code is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This source code is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
  more details.

 -----------------------------------------------------------------------------
*/

#include <mex.h>            // Matlab Mex Functions

#include <sba.h>            // Sparse Bundle Adjustement
#include <sba_types.h>

#include "errorMx.h"        // To Print out Errors
#include "readparams.h"     // Read Calibration, Cameras, Points from files

#define ERROR_HEADER  "MATLAB:msbaReadMx:Input\n"

// Identify the input parameters by it's index
enum { CALIBRATIONFILENAME=0, CAMERASFILENAME, POINTSFILENAME };
// Identify the output parameters by it's index
enum { CALIBRATION=0, CAMERAS, POINTS };

// Mex function
//
void mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  // Parameter checks
  if ( nlhs != 3 || nrhs != 3 )
    mexErrMsgTxt ( ERROR_HEADER "Usage: [Calibration Cameras Points] = msbaReadMx ( CalibrationFileName, CamerasFileName, PointsFileName )" );

  // Get the File Name lenghts
  sint SCalibrationFileName = mxGetN ( prhs[CALIBRATIONFILENAME] );
  sint SCamerasFileName = mxGetN ( prhs[CAMERASFILENAME] );
  sint SPointsFileName = mxGetN ( prhs[POINTSFILENAME] );

  // Parameters must be strings
  if ( mxGetM ( prhs[CALIBRATIONFILENAME] ) != 1 || SCalibrationFileName <= 0 || !mxIsChar ( prhs[CALIBRATIONFILENAME] ) )
    mexErrMsgTxt ( ERROR_HEADER "Calibration File Name must be a string!" );
  if ( mxGetM ( prhs[CAMERASFILENAME] ) != 1 || SCamerasFileName <= 0 || !mxIsChar ( prhs[CAMERASFILENAME] ) )
    mexErrMsgTxt ( ERROR_HEADER "Cameras File Name must be a string!" );
  if ( mxGetM ( prhs[POINTSFILENAME] ) != 1 || SPointsFileName <= 0 || !mxIsChar ( prhs[POINTSFILENAME] ) )
    mexErrMsgTxt ( ERROR_HEADER "Points File Name must be a string!" );

  // Reserve space for the File Names
  char *CalibrationFileName = (char *)mxMalloc ( SCalibrationFileName + 1 );
  char *CamerasFileName = (char *)mxMalloc ( SCamerasFileName + 1 );
  char *PointsFileName = (char *)mxMalloc ( SPointsFileName + 1 );

  if ( CalibrationFileName == NULL || CamerasFileName == NULL || PointsFileName == NULL )
    mexErrMsgTxt ( ERROR_HEADER "Dynamic memory couldn't be allocated!" );

  // Get the File Names
  mxGetString ( prhs[CALIBRATIONFILENAME], CalibrationFileName, SCalibrationFileName + 1 );
  mxGetString ( prhs[CAMERASFILENAME], CamerasFileName, SCamerasFileName + 1 );
  mxGetString ( prhs[POINTSFILENAME], PointsFileName, SPointsFileName + 1 );

  // Constants
  const sint cnp = 7;              // Pose number of elements (q1, q2, q3, q4, X, Y, Z)
  const sint mnp = 2;              // 2D Point number of elements (x, y)
  const sint pnp = 3;              // 3D Point number of elements (X, Y, Z)

  double ical[9];                 // Sotre the calibration
  double *motstruct, *imgpts;     // To store Cameras, 3D and 2D points
  sint ncams, n3Dpts, n2Dprojs;   // To store counters
  char *vmask;                    // Store mask where a 3D point is imaged
  double *Pr;                     // Temporary pointer
  double *PtMotstruct, *Ptimgpts; // To scan the Data Structures
  sint i, j;

  // Read the Calibration Matrix
  if ( !readCalibParams ( CalibrationFileName, ical ) )
  {
    char Buffer[STR_ERROR_BUFFER_SIZE+1];    // Buffer to print errors

    resumeStr ( Buffer, STR_ERROR_BUFFER_SIZE, CalibrationFileName, SCalibrationFileName );
    printfmexErrMsgTxt ( ERROR_HEADER "Error loading Calibration file '%s'!)", Buffer );
  }

  // Read the Cameras and the Points
  if ( !readInitialSBAEstimate ( CamerasFileName, PointsFileName, &ncams, &n3Dpts, &n2Dprojs, &motstruct, &imgpts, &vmask ) )
  {
    char BufferA[STR_ERROR_BUFFER_SIZE+1]; // Buffer to print errors
    char BufferB[STR_ERROR_BUFFER_SIZE+1];

    resumeStr ( BufferA, STR_ERROR_BUFFER_SIZE, CamerasFileName, SCamerasFileName );
    resumeStr ( BufferB, STR_ERROR_BUFFER_SIZE, PointsFileName, SPointsFileName );
    printfmexErrMsgTxt ( ERROR_HEADER "Error loading Camera's file '%s' or Points file '%s'!", BufferA, BufferB );
  }

  // Create Calibration Matrix
  plhs[CALIBRATION] = mxCreateDoubleMatrix ( 3, 3, mxREAL );

  // The calibration is in rows, in matlab is stored by columns => Transpose
  Pr = (double *)mxGetPr ( plhs[CALIBRATION] );
  *Pr++ = ical[0];  *Pr++ = ical[3];  *Pr++ = ical[6];
  *Pr++ = ical[1];  *Pr++ = ical[4];  *Pr++ = ical[7];
  *Pr++ = ical[2];  *Pr++ = ical[5];  *Pr++ = ical[8];

  // To scan structures and preserve original pointers
  PtMotstruct = motstruct;
  Ptimgpts = imgpts;

  // Create Calibration Matrix
  plhs[CAMERAS] = mxCreateDoubleMatrix ( cnp, ncams, mxREAL );
  Pr = (double *)mxGetPr ( plhs[CAMERAS] );
  for ( i = 0; i < ncams * cnp; i++ )
     *Pr++ = *PtMotstruct++;

  // Create the Cell Array
  plhs[POINTS] = mxCreateCellMatrix ( 2, n3Dpts );

  sint n2Dpts;
  char *pts2DOffs;
  char *prvmask;
  mxArray *CurrCell;

  // Initial position to vmask
  pts2DOffs = vmask;

  // Scan all the 3D Points
  for ( i = 0; i < n3Dpts; i++ )
  {  
    // Get the pointer to the current row
    prvmask = pts2DOffs;

    // Count how many 2D points are associated to the current 3D point
    n2Dpts = 0;
    for ( j = 0; j < ncams; j++, prvmask++ )
      n2Dpts += *prvmask ? 1 : 0;
  
    // Allocate space to store the 3D Point: X, Y, Z
    CurrCell = mxCreateDoubleMatrix ( pnp, 1, mxREAL );
    Pr = mxGetPr ( CurrCell );

    // Copy the X, Y, Z
    for ( j = 0; j < pnp; j++ )
      *Pr++ = *PtMotstruct++;

    // Assign to the data to the Current Cell into the Cell-Array
    mxSetCell ( plhs[POINTS], i * 2 + 0, CurrCell );

    // Create space for the set of 2D points and it's frame: N, x, y
    CurrCell = mxCreateDoubleMatrix ( 1 + mnp, n2Dpts, mxREAL );
    Pr = mxGetPr ( CurrCell );

    // Get the pointer to the current row
    prvmask = pts2DOffs;

    // Copy the set of 2D Points if they are associated to the 3D point
    for ( j = 0; j < ncams; j++, prvmask++ )
      if ( *prvmask )
      {
        *Pr++ = j + 1;         // N -> Preserve Matlab indices
        *Pr++ = *Ptimgpts++;   // x
        *Pr++ = *Ptimgpts++;   // y
      }

    // Assign the Cell
    mxSetCell ( plhs[POINTS], i * 2 + 1, CurrCell );

    // Pointer to next vmask row
    pts2DOffs += ncams;
  }

  // Free Allocated Memory
  mxFree ( CalibrationFileName );
  mxFree ( CamerasFileName );
  mxFree ( PointsFileName );

  free ( motstruct );
  free ( imgpts );
  free ( vmask );

  return;
}
