/*
  Autor(s)      : Jordi Ferrer Plana
  e-mail        : jferrerp@eia.udg.edu
  Branch        : Computer Vision

  Working Group : Underwater Vision Lab
  Project       : -

  Homepage      : http://porcsenglar.udg.edu

  Module        : Checking parameters.

  File          : msbaChecks.cpp
  Date          : 11/10/2007 - 20/04/2009

  Compiler      : MATLAB >= 7.0 & g++ >= 4.x
  Libraries     : -

  Notes         : - File written using ISO-8859-1 encoding.

 -----------------------------------------------------------------------------

  Copyright (C) 2005-2007 by Jordi Ferrer Plana

  This source code is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This source code is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
  more details.

 -----------------------------------------------------------------------------
*/

#ifndef __MSBACHECKS_H__
#define __MSBACHECKS_H__

#include <sba_types.h>

// Function to check the cell array content
//
void checkPoints ( const char *ErrorHeader, const char *CellContentMsg,
                   const mxArray *Set, const sint Num3DPoints, const sint MaxFrames,
                   const bool IsStereo = false );

// Calc the total number of 2D projected points. The mxArray content must be
// checked before.
//
sint calcNum2DProj ( const mxArray *Set, const sint Num3DPoints );

#endif
