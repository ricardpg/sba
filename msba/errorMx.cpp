/*
  Autor(s)      : Jordi Ferrer Plana
  e-mail        : jferrerp@eia.udg.edu
  Branch        : Computer Vision

  Working Group : Underwater Vision Lab
  Project       : -

  Homepage      : http://porcsenglar.udg.edu

  Module        : Generic error functions.

  File          : errorMx.cpp
  Date          : 11/10/2007 - 31/03/2009

  Compiler      : MATLAB >= 7.0 & g++ >= 4.x
  Libraries     : -

  Notes         : - File written using ISO-8859-1 encoding.

 -----------------------------------------------------------------------------

  Copyright (C) 2005-2006 by Jordi Ferrer Plana

  This source code is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This source code is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
  more details.

 -----------------------------------------------------------------------------
*/

#include <cstdarg>          // STL: Variable parameter lists.
#include <cstdio>

#include <mex.h>            // Matlab Mex Functions

#include <sba_types.h>

#include "errorMx.h"        // Definition

void printfmexErrMsgTxt ( const char *Format, ... )
{
  va_list VarPar;                    // Variable parameter list
  char Str[STR_ERROR_BUFFER_SIZE+1]; // Buffer String

  va_start ( VarPar, Format );       // Init Variable parameter list
  vsprintf ( Str, Format, VarPar );  // Print String

  mexErrMsgTxt ( Str );
  
  va_end ( VarPar );                 // Free variable parameter list
}

void printfmexWarnMsgTxt ( const char *Format, ... )
{
  va_list VarPar;                    // Variable parameter list
  char Str[STR_ERROR_BUFFER_SIZE+1]; // Buffer String

  va_start ( VarPar, Format );       // Init Variable parameter list
  vsprintf ( Str, Format, VarPar );  // Print String

  mexWarnMsgTxt ( Str );
  
  va_end ( VarPar );                 // Free variable parameter list
}

void resumeStr ( char *Dst, const suint DstSize,
                 const char *Src, suint SrcSize )
{
  suint i;

  if ( DstSize < SrcSize )
  {
    suint HalfSize;
    suint isEven;
      
    switch ( DstSize )
    {
      case 3: *Dst++ = '.';
      case 2: *Dst++ = '.';
      case 1: *Dst++ = '.';
      case 0: break;

      default:
        isEven = ( DstSize & 1 ) ? 0 : 1;

        // Left part
        HalfSize = ( DstSize - 3 ) / 2;
        for ( i = 0; i < (HalfSize + isEven); i++ )
          *Dst++ = Src[i];

        // Middle part
        *Dst++ = '.'; *Dst++ = '.'; *Dst++ = '.';

        // Right part
        for ( i = 0; i < HalfSize; i++ )
          *Dst++ = Src[SrcSize-HalfSize+i];
        break;
    }
  }
  else
    for ( i = 0; i < SrcSize; i++ ) *Dst++ = *Src++;

  *Dst++ = 0;
}
