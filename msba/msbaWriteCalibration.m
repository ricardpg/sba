%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Write the Monocular Sparse Bundle Adjustement Calibration
%                  structure.
%
%  File          : msbaWriteCalibration.m
%  Date          : 30/01/2007 - 13/02/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  msbaWriteCalibration Write the content of the Calibration matrix to a file.
%
%      msbaWriteCalibration ( Calibration, FileName )
%
%     Input Parameters:
%      Calibration: 3x3 intrinsic parameters matrix.
%      FileName: String containing the full file path where the data must be
%                stored.
%
%     Output Parameters:
%
%

function msbaWriteCalibration ( Calibration, FileName )
  % Test the input parameters
  error ( nargchk ( 2, 2, nargin ) );
  error ( nargoutchk ( 0, 0, nargout ) );
  
  [Rows, Columns] = size ( Calibration );
  if ~isnumeric ( Calibration ) || Rows ~= 3 || Columns ~= 3;
    error ( 'MATLAB:msbaWriteCalibration:Input', 'Calibration must be a 3x3 real matrix!' );
  end

  FileFID = fopen ( FileName, 'w' );
  if FileFID < 0; error ( 'MATLAB:msbaWriteCalibration:Input', [ 'Cannot open "' FileName '" to write Calibration structure!' ] ); end

  % Write calibration data
  fprintf ( FileFID, '%.16f\t%.16f\t%.16f\n%.16f\t%.16f\t%.16f\n%.16f\t%.16f\t%.16f\n', ...
            Calibration(1,1), Calibration(1,2), Calibration(1,3), ...
            Calibration(2,1), Calibration(2,2), Calibration(2,3), ...
            Calibration(3,1), Calibration(3,2), Calibration(3,3) );

  fclose ( FileFID );
end
