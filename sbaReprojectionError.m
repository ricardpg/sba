%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Compute the reprojection of the data in the Sparse
%                  Bundle Adjustement Structure.
%
%  File          : sbaReprojectionError.m
%  Date          : 19/02/2007 - 06/06/2008
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2008 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  sbaReprojectionError Compute the reprojection error for the set of 3D
%                       points in the Points structure using the K as the
%                       camera projection matrix and Cameras as the set of
%                       camera positions.
%
%      [E, Se] = sbaReprojectionError ( K, Cameras, Points )
%
%     Input Parameters:
%      K: 3x3 Intrisic Camera parameters matrix.
%      Cameras: 7xn Camera poses in format (qr, q1, q2, q3, X, Y, Z).
%      Points: 2xp cell-array. See msba.m
%
%     Output Parameters:
%      E: 2xp cell array. In the first row the reprojection error for each
%         3D point in respected to the 2D imaged point is computed. In the
%         second row, the sum of squared errors between all the 3D Points
%         and the correspondent 2D imaged points is computed.
%      Se: Sum of squared errors for all the 2D Imaged points and their
%          correspondent 3D points.
%

function [E, Se] = sbaReprojectionError ( K, Cameras, Points )
  % Test the input parameters
  error ( nargchk ( 3, 3, nargin ) );
  error ( nargoutchk ( 2, 2, nargout ) );

  % Check the Points Structure Content
  sbaCheckPoints ( Points, false, false );

  % Test the input size
  [Cr, numpts3D] = size ( Points );
  if ~iscell ( Points ) || Cr ~= 2 || numpts3D <= 0;
    error ( 'MATLAB:sbaReprojectionError:Input', ErrorStr );
  end

  % Convert to Homogeneous coordinates
  H = PoseToHomo ( Cameras );

  % The resulting errors
  E = cell ( 2, numpts3D );

  N = sbaNumProjections ( Points, false );
  % Error vector => Each reprojection has 2 residuals: Ex, Ey
  AllErr = zeros ( 1, N * 2 );
  k = 1;
  
  % Intrinsic parameters to project the 3D points
  Kp = [ K [0; 0; 0] ];
  for i = 1 : numpts3D;
    % Test
    NumProj = size ( Points{2,i}, 2 );
    % New cell in E:
    E{1, i} = zeros ( 1, NumProj );
    % The accumulated for the set of 3D points
    Err3DSet = 0;
    for j = 1 : NumProj;
      % Get Camera Index
      Cam = Points{2,i}(1,j);
      % Project the Point
      P2D = Kp * ( H(:, :, Cam) * [Points{1,i}(:, 1); 1] );
      % Eliminate Scale
      P2D(1:3) = P2D(1:3) / P2D(3);

      % Store the error
      Diff = Points{2,i}(2:3,j) - P2D(1:2);
      
      % Write into the error vector
      AllErr(k:k+1) = Diff;
      k = k + 2;

      E{1, i}(1, j) = sum ( Diff .* Diff );
      Err3DSet = Err3DSet + E{1, i}(1, j);
    end
    % The Accumulated Error for the set of 3D points
    E{2, i} = Err3DSet;
  end

  % The Sum of squared errors
  Se = sum ( AllErr .* AllErr );
end
