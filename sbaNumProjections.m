%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Compute the number of projections in the Sparse Bundle
%                  Adjustement Structure.
%
%  File          : sbaNumProjections.m
%  Date          : 22/02/2007 - 12/10/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  sbaNumProjections Compute the number of projections in the Monocular
%                    Sparse Bundle Adjustement Structure.
%
%      N = sbaNumProjections ( Points )
%
%     Input Parameters:
%      Points: See msba.m
%      IsStereo: If true, the Stereo is taken into account otherwise
%                monouclar case is assumed. In Stereo, Points structure
%                must have 2 x 2D points in the rows of the cell.
%
%     Output Parameters:
%      N: Number of projections in Points.
%
%

function N = sbaNumProjections ( Points, IsStereo )
  % Test the input parameters
  error ( nargchk ( 2, 2, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % Check the msba/ssba Points Structure Content
  sbaCheckPoints ( Points, false, IsStereo );

  % Test the input size
  [Cr, numpts3D] = size ( Points );
  if ~iscell ( Points ) || Cr ~= 2 || numpts3D <= 0;
    error ( 'MATLAB:sbaNumProjections:Input', ErrorStr );
  end

  N = 0;
  for i = 1 : numpts3D;
      N = N + size ( Points{2,i}, 2 );
  end
end
