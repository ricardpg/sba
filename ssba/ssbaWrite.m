%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Write the Stereo Sparse Bundle Adjustement structures.
%
%  File          : ssbaWrite.m
%  Date          : 11/10/2007 - 12/10/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  ssbaWrite Write the content of the Calibration, Cameras and Points structures
%            to files.
%
%      ssbaWrite ( Calibration, Cameras, Points,
%                  CalibrationFileName, CamerasFileName, PointsFileName )
%
%     Input Parameters:
%      Calibration: 3x3 intrinsic parameters matrix.
%      Cameras: 7xK matrix containing each camera pose expressed as a
%               quaternion for the rotation and X, Y, Z for the position.
%      Points: The same than msba.m but with 2 x 2D points instead of a single
%              point in the second row of the cell-array.
%      CalibrationFileName: String containing the full file path where the
%                           Calibration matrix must be stored.
%      CamerasFileName: String containing the full file path where the
%                       Cameras structure must be stored.
%      PointsFileName: String containing the full file path where the
%                      Points structure must be stored.
%
%     Output Parameters:
%
%

function ssbaWrite ( Calibration, Cameras, Points, ...
                     CalibrationFileName, CamerasFileName, PointsFileName )
  % Test the input parameters
  error ( nargchk ( 6, 6, nargin ) );
  error ( nargoutchk ( 0, 0, nargout ) );
  
  % Write Input Data to Files
  ssbaWriteCalibration ( Calibration, CalibrationFileName );
  sbaWriteCameras ( Cameras, CamerasFileName );
  sbaWritePoints ( Points, PointsFileName, true );
end
