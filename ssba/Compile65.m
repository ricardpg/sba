%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Generate Matlab Mex Files for Matlab 6.5.
%
%  File          : Compile65.m
%  Date          : 19/11/2007 - 27/01/2009
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%                  - This script should be run in Matlab from the msba/
%                    folder.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%

% Detect Matlab Version
MatVer = version ( '-release' );

% 13 for 6.5 or smaller for lower versions and 20 for others (2006a, 2006b, 2007a, 2007b, 2008b)
Major = str2double ( MatVer(1:2) );

if Major > 13;
  error ( 'MATLAB:Compile65:Check', ...
          'This script is only for Matlab 6.5 or lower versions. Use Compile.m instead!' );
end

% Sba Folders
SbaFolder = '../3rdParty/sba-1.5';
SbaDemoFolder = [SbaFolder '/demo'];
MSbaFolder = '../msba';
SSbaFolder = 'bin';

% Add Library path to LAPACK in Windows Architectures
if Windows;
  % Add path to lapack
  LapackFolder = '../3rdParty/clapack/';
  if Nbit == 32;
    LapackFolder = [ LapackFolder 'win32' ];
  elseif Nbit == 64;
    LapackFolder = [ LapackFolder 'win64' ];
  else
    LapackFolder = '';
  end

  LapackLibrary = 'clapack';
  WindowsLibraries = [ LapackFolder '/' LapackLibrary '.lib' ];
  WindowsLibraries = [ WindowsLibraries ' ' LapackFolder '/blas.lib' ];
  WindowsLibraries = [ WindowsLibraries ' ' LapackFolder '/libF77.lib' ];
  WindowsLibraries = [ WindowsLibraries ' ' LapackFolder '/libI77.lib' ];
  WindowsLibraries = [ WindowsLibraries ' ' SbaFolder '/' SbaLibrary '.lib' ];

  % Empty
  LibraryPath = '';
  lLapackLibrary = '';
  lSbaLibrary = '';  
else
  % Empty
  WindowsLibraries = '';

  % Library path to Sba
  LibraryPath = [ ' -L' SbaFolder ];

  LapackLibrary = 'lapack';
  lLapackLibrary = [ ' -l' LapackLibrary ];

  lSbaLibrary = [ ' -l' SbaLibrary ];
end

% Compile
disp ( sprintf ( 'Compiling ssbaiMx.cpp' ) );
Cmd = ['mex -I' SbaFolder ' -I' SbaDemoFolder ' -I' MSbaFolder LibraryPath lLapackLibrary lSbaLibrary ' ssbaiMx.cpp ' MSbaFolder '/msbaChecks.cpp ' MSbaFolder '/errorMx.cpp ' SSbaFolder '/ssba.cpp ' SSbaFolder '/readparams.cpp ' SbaDemoFolder '/imgproj.c ' WindowsLibraries];
disp ( sprintf ( ['\n' Cmd '\n' ] ) );
eval ( Cmd );

disp ( sprintf ( 'Compiling ssbaReadMx.cpp' ) );
Cmd = ['mex -I' SbaFolder ' -I' SbaDemoFolder ' -I' MSbaFolder LibraryPath lSbaLibrary ' ssbaReadMx.cpp readparams.cpp ' MSbaFolder '/errorMx.cpp ' WindowsLibraries];
disp ( sprintf ( ['\n' Cmd '\n' ] ) );
eval ( Cmd );
