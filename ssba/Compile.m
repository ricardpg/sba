%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Generate Matlab Mex Files.
%
%  File          : Compile.m
%  Date          : 11/10/2007 - 27/01/2009
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%                  - This script should be run in Matlab from the ssba/
%                    folder.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2008 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%

% Detect Matlab Version
MatVer = version ( '-release' );

% 13 for 6.5 or smaller for lower versions and 20 for others (2006a, 2006b, 2007a, 2007b, 2008a)
Major = str2double ( MatVer(1:2) );

if Major <= 13;
  error ( 'MATLAB:Compile65:Check', ...
          'This script is only for Matlab > 6.5 or higher versions. Use Compile65.m instead!' );
end

% Detect Platform
Arch = computer;

if strcmpi ( Arch, 'pcwin' ); Windows = 1; Nbit = 32;
elseif strcmpi ( Arch, 'pcwin64' ); Windows = 1; Nbit = 64;
else Windows = 0;
end

% Sba Folders
SbaFolder = '../3rdParty/sba-1.5';
SbaDemoFolder = [SbaFolder '/demo'];
MSbaFolder = '../msba';
SSbaFolder = 'bin';

% Library path to Sba
LibraryPath = [ ' -L' SbaFolder ];
SbaLibrary = 'sba';
lSbaLibrary = [ ' -l' SbaLibrary ];

% Add Library path to LAPACK in Windows Architectures
if Windows;
  % Add path to lapack
  LapackFolder = '../3rdParty/clapack/';
  if Nbit == 32;
    LapackFolder = [ LapackFolder 'win32' ];
  elseif Nbit == 64;
    LapackFolder = [ LapackFolder 'win64' ];
  else
    LapackFolder = '';
  end
  LibraryPath = [ LibraryPath ' -L' LapackFolder ];

  LapackLibrary = 'clapack';
  lLapackLibrary = [ ' -l' LapackLibrary ' -lblas -llibF77 -llibI77' ];

else
  LapackLibrary = 'lapack';
  lLapackLibrary = [ ' -l' LapackLibrary ];
end

% Compile
disp ( sprintf ( 'Compiling ssbaiMx.cpp' ) );
Cmd = ['mex -I' SbaFolder ' -I' SbaDemoFolder ' -I' MSbaFolder ' -I' MSbaFolder '/bin' LibraryPath  lLapackLibrary lSbaLibrary ' ssbaiMx.cpp ' MSbaFolder '/bin/quat.cpp ' MSbaFolder '/msbaChecks.cpp ' MSbaFolder '/errorMx.cpp ' SSbaFolder '/ssba.cpp ' SSbaFolder '/readparams.cpp ' SbaDemoFolder '/imgproj.c'];
disp ( sprintf ( ['\n' Cmd '\n' ] ) );
eval ( Cmd );

disp ( sprintf ( 'Compiling ssbaReadMx.cpp' ) );
Cmd = ['mex -I' SbaFolder ' -I' SbaDemoFolder ' -I' MSbaFolder LibraryPath lLapackLibrary ' ssbaReadMx.cpp readparams.cpp ' MSbaFolder '/errorMx.cpp'];
disp ( sprintf ( ['\n' Cmd '\n' ] ) );
eval ( Cmd );
