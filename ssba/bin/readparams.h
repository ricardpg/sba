#ifndef __READPARAMS_H__
#define __READPARAMS_H__

#include <sba_types.h>

void readCameraParams ( FILE *fp, double *params );
void readNpointsAndNprojections ( FILE *fp, sint *n3Dpts, sint *nprojs );
void readPointParamsAndProjections ( FILE *fp, double *params, double *projs, char *vmask, sint ncams );
void readInitialSSBAEstimate ( char *camsfname, char *ptsfname, sint *ncams, sint *n3Dpts, sint *n2Dprojs, double **motstruct, double **imgpts, char **vmask );
void readSCalibParams ( char *fname, double ical[9+3] );
void printMask ( const char *FileName, const char *vmask, const sint ncams, const sint n3Dpts );
void printSSBAData ( char *MotionFileName, char *StructFileName, char *ProjectionsFileName,
                    double *motstruct, sint ncams, sint n3Dpts, double *imgpts, sint n2Dprojs, char *vmask );

#endif
