#ifndef __MSBA_H__
#define __MSBA_H__

#include <sba_types.h>

/* Pointers to additional data, used for computed image projections and their jacobians */
extern struct globs_{
  double *intrcalib; /* the 5 intrinsic calibration parameters in the order [fu, u0, v0, ar, skew],
                      * where ar is the aspect ratio fv/fu.
                      * Used only when calibration is fixed for all cameras;
                      * otherwise, it is null and the intrinsic parameters are
                      * included in the set of motion parameters for each camera
                      */
  sint nccalib; /* number of calibration parameters that must be kept constant.
                * 0: all paremeters are free
                * 1: skew is fixed to its initial value, all other parameters vary (i.e. fu, u0, v0, ar)
                * 2: skew and aspect ratio are fixed to their initial values, all other parameters vary (i.e. fu, u0, v0)
                * 3: meaningless
                * 4: skew, aspect ratio and principal point are fixed to their initial values, only the focal length varies (i.e. fu)
                * >=5: meaningless
                * Used only when calibration varies among cameras
                */

  sint cnp, pnp, mnp; /* dimensions */

  double *ptparams; /* needed only when bundle adjusting for camera parameters only */
  double *camparams; /* needed only when bundle adjusting for structure parameters only */
} globs;


void img_projRTS(sint j, sint i, double *aj, double *bi, double *xij, void *adata);
void img_projRTS_jac(sint j, sint i, double *aj, double *bi, double *Aij, double *Bij, void *adata);
void img_projRT(sint j, sint i, double *aj, double *xij, void *adata);
void img_projRT_jac(sint j, sint i, double *aj, double *Aij, void *adata);
void img_projS(sint j, sint i, double *bi, double *xij, void *adata);
void img_projS_jac(sint j, sint i, double *bi, double *Bij, void *adata);
void img_projsRTS_x(double *p, struct sba_crsm *idxij, sint *rcidxs, sint *rcsubs, double *hx, void *adata);
void img_projsRTS_jac_x(double *p, struct sba_crsm *idxij, sint *rcidxs, sint *rcsubs, double *jac, void *adata);
void img_projsRT_x(double *p, struct sba_crsm *idxij, sint *rcidxs, sint *rcsubs, double *hx, void *adata);
void img_projsRT_jac_x(double *p, struct sba_crsm *idxij, sint *rcidxs, sint *rcsubs, double *jac, void *adata);
void img_projsS_x(double *p, struct sba_crsm *idxij, sint *rcidxs, sint *rcsubs, double *hx, void *adata);
void img_projsS_jac_x(double *p, struct sba_crsm *idxij, sint *rcidxs, sint *rcsubs, double *jac, void *adata);

void img_projKRTS(sint j, sint i, double *aj, double *bi, double *xij, void *adata);
void img_projKRTS_jac(sint j, sint i, double *aj, double *bi, double *Aij, double *Bij, void *adata);
void img_projKRT(sint j, sint i, double *aj, double *xij, void *adata);
void img_projKRT_jac(sint j, sint i, double *aj, double *Aij, void *adata);
void img_projKS(sint j, sint i, double *bi, double *xij, void *adata);
void img_projKS_jac(sint j, sint i, double *bi, double *Bij, void *adata);
void img_projsKRTS_x(double *p, struct sba_crsm *idxij, sint *rcidxs, sint *rcsubs, double *hx, void *adata);
void img_projsKRTS_jac_x(double *p, struct sba_crsm *idxij, sint *rcidxs, sint *rcsubs, double *jac, void *adata);
void img_projsKRT_x(double *p, struct sba_crsm *idxij, sint *rcidxs, sint *rcsubs, double *hx, void *adata);
void img_projsKRT_jac_x(double *p, struct sba_crsm *idxij, sint *rcidxs, sint *rcsubs, double *jac, void *adata);
void img_projsKS_x(double *p, struct sba_crsm *idxij, sint *rcidxs, sint *rcsubs, double *hx, void *adata);
void img_projsKS_jac_x(double *p, struct sba_crsm *idxij, sint *rcidxs, sint *rcsubs, double *jac, void *adata);

#endif
