/* Euclidean bundle adjustment demo using the sba package */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <cassert>

#include <sba.h>
#include <sba_types.h>

#ifdef __cplusplus
extern "C" {
#endif
  #include <eucsbademo.h>
#ifdef __cplusplus
}
#endif

#include "ssba.h"

/* pointers to additional data, used for computed image projections and their jacobians */
struct globs_ globs;

/* Routines to estimate the estimated measurement vector (i.e. "func") and
 * its sparse jacobian (i.e. "fjac") needed in BA. Code below makes use of the
 * routines calcImgProj() and calcImgProjJacRTS() which compute the predicted
 * projection & jacobian of a SINGLE 3D point (see imgproj.c).
 * In the terminology of TR-340, these routines compute Q and its jacobians A=dQ/da, B=dQ/db.
 * Notice also that what follows is two pairs of "func" and corresponding "fjac" routines.
 * The first is to be used in full (i.e. motion + structure) BA, the second in 
 * motion only BA.
 */


/**********************************************************************/
/* MEASUREMENT VECTOR AND JACOBIAN COMPUTATION FOR THE SIMPLE DRIVERS */
/**********************************************************************/

/* FULL BUNDLE ADJUSTMENT, I.E. SIMULTANEOUS ESTIMATION OF CAMERA AND STRUCTURE PARAMETERS */

/* Given the parameter vectors aj and bi of camera j and point i, computes in xij the
 * predicted projection of point i on image j
 */
void img_projRTS ( sint j, sint i, double *aj, double *bi, double *xij, void *adata )
{
  double *Kcalib;
  struct globs_ *gl;

  gl = (struct globs_ *)adata;
  Kcalib = gl->intrcalib;

  calcImgProj ( Kcalib, aj, aj + 3, bi, xij );  // 3 is the quaternion's length

// Jordi Ferrer, 02/02/2009
  double t[3];

  t[0] = aj[3] + Kcalib[5]; t[1] = aj[4] + Kcalib[6]; t[2] = aj[5] + Kcalib[7];
  calcImgProj ( Kcalib, aj, t, bi, xij + 2 );  // 3 is the quaternion's length
// Jordi Ferrer, 02/02/2009
}

/* Given the parameter vectors aj and bi of camera j and point i, computes in Aij, Bij the
 * jacobian of the predicted projection of point i on image j
 */
void img_projRTS_jac ( sint j, sint i, double *aj, double *bi, double *Aij, double *Bij, void *adata )
{
  double *Kcalib;
  struct globs_ *gl;
  
  gl = (struct globs_ *)adata;
  Kcalib = gl->intrcalib;

  calcImgProjJacRTS ( Kcalib, aj, aj + 3, bi, (double (*)[6])Aij, (double (*)[3])Bij );  // 3 is the quaternion's length

// Jordi Ferrer, 02/02/2009
  double t[3];

  t[0] = aj[3] + Kcalib[5]; t[1] = aj[4] + Kcalib[6]; t[2] = aj[5] + Kcalib[7];
  calcImgProjJacRTS ( Kcalib, aj, t, bi, (double (*)[6])(Aij + 12), (double (*)[3])(Bij + 6) );  // 3 is the quaternion's length
// Jordi Ferrer, 02/02/2009
}

/* BUNDLE ADJUSTMENT FOR CAMERA PARAMETERS ONLY */

/* Given the parameter vector aj of camera j, computes in xij the
 * predicted projection of point i on image j
 */
void img_projRT ( sint j, sint i, double *aj, double *xij, void *adata )
{
  double *Kcalib, *ptparams;
  struct globs_ *gl;

  gl = (struct globs_ *)adata;
  Kcalib = gl->intrcalib;
  ptparams = gl->ptparams + i * gl->pnp;

  calcImgProj ( Kcalib, aj, aj + 3, ptparams, xij );  // 3 is the quaternion's length

// Jordi Ferrer, 02/02/2009
  double t[3];

  t[0] = aj[3] + Kcalib[5]; t[1] = aj[4] + Kcalib[6]; t[2] = aj[5] + Kcalib[7];
  calcImgProj ( Kcalib, aj, t, ptparams, xij + 2 );  // 3 is the quaternion's length
// Jordi Ferrer, 02/02/2009
}

/* Given the parameter vector aj of camera j, computes in Aij
 * the jacobian of the predicted projection of point i on image j
 */
void img_projRT_jac ( sint j, sint i, double *aj, double *Aij, void *adata )
{
  double *Kcalib, *ptparams;
  struct globs_ *gl;

  gl = (struct globs_ *)adata;
  Kcalib = gl->intrcalib;
  ptparams = gl->ptparams + i * gl->pnp;

  calcImgProjJacRT ( Kcalib, aj, aj + 3, ptparams, (double (*)[6])Aij );  // 3 is the quaternion's length

// Jordi Ferrer, 02/02/2009
  double t[3];

  t[0] = aj[3] + Kcalib[5]; t[1] = aj[4] + Kcalib[6]; t[2] = aj[5] + Kcalib[7];
  calcImgProjJacRT ( Kcalib, aj, t, ptparams, (double (*)[6])(Aij + 12) );  // 3 is the quaternion's length
// Jordi Ferrer, 02/02/2009
}

/* BUNDLE ADJUSTMENT FOR STRUCTURE PARAMETERS ONLY */

/* Given the parameter vector bi of point i, computes in xij the
 * predicted projection of point i on image j
 */
void img_projS ( sint j, sint i, double *bi, double *xij, void *adata )
{
  double *Kcalib, *aj;
  struct globs_ *gl;

  gl = (struct globs_ *)adata;
  Kcalib = gl->intrcalib;
  aj = gl->camparams + j * gl->cnp;

  calcImgProj ( Kcalib, aj, aj + 3, bi, xij );  // 3 is the quaternion's length

// Jordi Ferrer, 02/02/2009
  double t[3];

  t[0] = aj[3] + Kcalib[5]; t[1] = aj[4] + Kcalib[6]; t[2] = aj[5] + Kcalib[7];
  calcImgProj ( Kcalib, aj, t, bi, xij + 2 );  // 3 is the quaternion's length
// Jordi Ferrer, 02/02/2009
}

/* Given the parameter vector bi of point i, computes in Bij
 * the jacobian of the predicted projection of point i on image j
 */
void img_projS_jac ( sint j, sint i, double *bi, double *Bij, void *adata )
{
  double *Kcalib, *aj;
  struct globs_ *gl;

  gl = (struct globs_ *)adata;
  Kcalib = gl->intrcalib;
  aj = gl->camparams + j * gl->cnp;

  calcImgProjJacS ( Kcalib, aj, aj + 3, bi, (double (*)[3])Bij );  // 3 is the quaternion's length

// Jordi Ferrer, 02/02/2009
  double t[3];

  t[0] = aj[3] + Kcalib[5]; t[1] = aj[4] + Kcalib[6]; t[2] = aj[5] + Kcalib[7];
  calcImgProjJacS ( Kcalib, aj, t, bi, (double (*)[3])(Bij + 6) );  // 3 is the quaternion's length
// Jordi Ferrer, 02/02/2009
}



/**********************************************************************/
/* MEASUREMENT VECTOR AND JACOBIAN COMPUTATION FOR THE EXPERT DRIVERS */
/**********************************************************************/

/* FULL BUNDLE ADJUSTMENT, I.E. SIMULTANEOUS ESTIMATION OF CAMERA AND STRUCTURE PARAMETERS */

/* Given a parameter vector p made up of the 3D coordinates of n points and the parameters of m cameras, compute in
 * hx the prediction of the measurements, i.e. the projections of 3D points in the m images. The measurements
 * are returned in the order (hx_11^T, .. hx_1m^T, ..., hx_n1^T, .. hx_nm^T)^T, where hx_ij is the predicted
 * projection of the i-th point on the j-th camera.
 * Notice that depending on idxij, some of the hx_ij might be missing
 *
 */
void img_projsRTS_x ( double *p, struct sba_crsm *idxij, sint *rcidxs, sint *rcsubs, double *hx, void *adata )
{
// Jordi Ferrer, 02/02/2009
  double t[3];
// Jordi Ferrer, 02/02/2009
  sint i, j, cnp, pnp, mnp;
  double *pa, *pb, *pt, *ppt, *pmeas, *Kcalib;
  // sint n;
  sint m, nnz;
  struct globs_ *gl;

  gl = (struct globs_ *)adata;
  Kcalib = gl->intrcalib;
  cnp = gl->cnp;  // 3 Rotation + 3 Translation Parametres
  pnp = gl->pnp;  // Euclidean 3D points
  mnp = gl->mnp;  // Imaged points are 4D

  // n = idxij->nr;
  m = idxij->nc;
  pa = p;              // Point to Cameras
  pb = p + m * cnp;    // Point to 3D Point

  for ( j = 0; j < m; ++j )
  {
// Jordi Ferrer, 02/02/2009
    pt = pa + 3;  // Rotation Quaternion has 3 elements
    t[0] = pt[0] + Kcalib[5]; t[1] = pt[1] + Kcalib[6]; t[2] = pt[2] + Kcalib[7];
// Jordi Ferrer, 02/02/2009

    nnz = sba_crsm_col_elmidxs ( idxij, j, rcidxs, rcsubs );  // Find nonzero hx_ij, i = 0..n-1

    for ( i = 0; i < nnz; ++i )
    {
      ppt = pb + rcsubs[i] * pnp;
      pmeas = hx + idxij->val[rcidxs[i]] * mnp;      // Set pmeas to point to hx_ij

      calcImgProj ( Kcalib, pa, pt, ppt, pmeas );    // Evaluate Q in pmeas
// Jordi Ferrer, 02/02/2009
      calcImgProj ( Kcalib, pa, t, ppt, pmeas + 2 ); // Evaluate Q in pmeas
// Jordi Ferrer, 02/02/2009      
    }

    // j-th Camera Parameters
    pa += cnp;
  }
}

/* Given a parameter vector p made up of the 3D coordinates of n points and the parameters of m cameras, compute in
 * jac the jacobian of the predicted measurements, i.e. the jacobian of the projections of 3D points in the m images.
 * The jacobian is returned in the order (A_11, ..., A_1m, ..., A_n1, ..., A_nm, B_11, ..., B_1m, ..., B_n1, ..., B_nm),
 * where A_ij=dx_ij/db_j and B_ij=dx_ij/db_i (see HZ).
 * Notice that depending on idxij, some of the A_ij, B_ij might be missing
 *
 */
void img_projsRTS_jac_x ( double *p, struct sba_crsm *idxij, sint *rcidxs, sint *rcsubs, double *jac, void *adata )
{
// Jordi Ferrer, 02/02/2009
  double t[3];
// Jordi Ferrer, 02/02/2009
  sint i, j, cnp, pnp, mnp;
  double *pa, *pb, *pt, *ppt, *pA, *pB, *Kcalib;
  // sint n;
  sint m, nnz, Asz, Bsz, ABsz, idx;
  struct globs_ *gl;

  gl = (struct globs_ *)adata;
  Kcalib = gl->intrcalib;
  cnp = gl->cnp;  // 3 Rotation + 3 Translation Parametres
  pnp = gl->pnp;  // Euclidean 3D points
  mnp = gl->mnp;  // Imaged points are 4D

  // n = idxij->nr;
  m = idxij->nc;
  pa = p;              // Point to Cameras
  pb = p + m * cnp;    // Point to 3D Point

  Asz = mnp * cnp;     // Sizes
  Bsz = mnp * pnp;
  ABsz = Asz + Bsz;

  for ( j = 0; j < m; ++j )
  {
// Jordi Ferrer, 02/02/2009
    pt = pa + 3;  // Rotation Quaternion has 3 elements
    t[0] = pt[0] + Kcalib[5]; t[1] = pt[1] + Kcalib[6]; t[2] = pt[2] + Kcalib[7];
// Jordi Ferrer, 02/02/2009

    nnz = sba_crsm_col_elmidxs ( idxij, j, rcidxs, rcsubs );  // Find nonzero hx_ij, i = 0..n-1

    for ( i = 0; i < nnz; ++i )
    {
      ppt = pb + rcsubs[i] * pnp;
      idx = idxij->val[rcidxs[i]];
      pA = jac + idx * ABsz;       // Set pA to point to A_ij
      pB = pA + Asz;               // Set pB to point to B_ij

      calcImgProjJacRTS ( Kcalib, pa, pt, ppt, (double (*)[6])pA, (double (*)[3])pB );             // Evaluate dQ/da, dQ/db in pA, pB
// Jordi Ferrer, 02/02/2009
      calcImgProjJacRTS ( Kcalib, pa, t, ppt, (double (*)[6])(pA + 12), (double (*)[3])(pB + 6) ); // Evaluate dQ/da, dQ/db in pA, pB
// Jordi Ferrer, 02/02/2009
    }

    // j-th Camera Parameters
    pa += cnp;
  }
}

/* BUNDLE ADJUSTMENT FOR CAMERA PARAMETERS ONLY */

/* Given a parameter vector p made up of the parameters of m cameras, compute in
 * hx the prediction of the measurements, i.e. the projections of 3D points in the m images.
 * The measurements are returned in the order (hx_11^T, .. hx_1m^T, ..., hx_n1^T, .. hx_nm^T)^T,
 * where hx_ij is the predicted projection of the i-th point on the j-th camera.
 * Notice that depending on idxij, some of the hx_ij might be missing
 *
 */
void img_projsRT_x ( double *p, struct sba_crsm *idxij, sint *rcidxs, sint *rcsubs, double *hx, void *adata )
{
// Jordi Ferrer, 02/02/2009
  double t[3];
// Jordi Ferrer, 02/02/2009
  sint i, j, cnp, pnp, mnp;
  double *pt, *ppt, *pmeas, *Kcalib, *ptparams;
  // sint n;
  sint m, nnz;
  struct globs_ *gl;

  gl = (struct globs_ *)adata;
  Kcalib = gl->intrcalib;
  cnp = gl->cnp;  // 3 Rotation + 3 Translation Parametres
  pnp = gl->pnp;  // Euclidean 3D points
  mnp = gl->mnp;  // Imaged points are 4D
  ptparams = gl->ptparams;

  // n = idxij->nr;
  m = idxij->nc;

  for ( j = 0; j < m; ++j )
  {
    pt = p + 3;  // Rotation Quaternion has 3 elements
    t[0] = pt[0] + Kcalib[5]; t[1] = pt[1] + Kcalib[6]; t[2] = pt[2] + Kcalib[7];

    nnz = sba_crsm_col_elmidxs ( idxij, j, rcidxs, rcsubs );  // Find nonzero hx_ij, i = 0..n-1

    for ( i = 0; i < nnz; ++i )
    {
      ppt = ptparams + rcsubs[i] * pnp;
      pmeas = hx + idxij->val[rcidxs[i]] * mnp;      // Set pmeas to point to hx_ij

      calcImgProj ( Kcalib, p, pt, ppt, pmeas );     // Evaluate Q in pmeas
// Jordi Ferrer, 02/02/2009
      calcImgProj ( Kcalib, p, t, ppt, pmeas + 2 );  // Evaluate Q in pmeas
// Jordi Ferrer, 02/02/2009
    }

    // j-th Camera Parameters
    p += cnp;
  }
}

/* Given a parameter vector p made up of the parameters of m cameras, compute in jac
 * the jacobian of the predicted measurements, i.e. the jacobian of the projections of 3D points in the m images.
 * The jacobian is returned in the order (A_11, ..., A_1m, ..., A_n1, ..., A_nm),
 * where A_ij=dx_ij/db_j (see HZ).
 * Notice that depending on idxij, some of the A_ij might be missing
 *
 */
void img_projsRT_jac_x ( double *p, struct sba_crsm *idxij, sint *rcidxs, sint *rcsubs, double *jac, void *adata )
{
// Jordi Ferrer, 02/02/2009
  double t[3];
// Jordi Ferrer, 02/02/2009
  sint i, j, cnp, pnp, mnp;
  double *pt, *ppt, *pA, *Kcalib, *ptparams;
  // sint n;
  sint m, nnz, Asz, idx;
  struct globs_ *gl;

  gl = (struct globs_ *)adata;
  Kcalib = gl->intrcalib;
  cnp = gl->cnp;  // 3 Rotation + 3 Translation Parametres
  pnp = gl->pnp;  // Euclidean 3D points
  mnp = gl->mnp;  // Imaged points are 4D
  ptparams = gl->ptparams;

  // n = idxij->nr;
  m = idxij->nc;
  Asz = mnp * cnp;

  for ( j = 0; j < m; ++j )
  {
    pt = p + 3;  // Rotation Quaternion has 3 elements
    t[0] = pt[0] + Kcalib[5]; t[1] = pt[1] + Kcalib[6]; t[2] = pt[2] + Kcalib[7];

    nnz = sba_crsm_col_elmidxs ( idxij, j, rcidxs, rcsubs );  // Find nonzero hx_ij, i = 0..n-1

    for ( i = 0; i < nnz; ++i )
    {
      ppt = ptparams + rcsubs[i] * pnp;
      idx = idxij->val[rcidxs[i]];
      pA = jac + idx * Asz;  // Set pA to point to A_ij

      calcImgProjJacRT ( Kcalib, p, pt, ppt, (double (*)[6])pA );        // Evaluate dQ/da in pA
// Jordi Ferrer, 02/02/2009
      calcImgProjJacRT ( Kcalib, p, t, ppt, (double (*)[6])(pA + 12) );  // Evaluate dQ/da in pA
// Jordi Ferrer, 02/02/2009      
    }

    // j-th Camera Parameters
    p += cnp;
  }
}

/* BUNDLE ADJUSTMENT FOR STRUCTURE PARAMETERS ONLY */

/* Given a parameter vector p made up of the 3D coordinates of n points and the parameters of m cameras, compute in
 * hx the prediction of the measurements, i.e. the projections of 3D points in the m images. The measurements
 * are returned in the order (hx_11^T, .. hx_1m^T, ..., hx_n1^T, .. hx_nm^T)^T, where hx_ij is the predicted
 * projection of the i-th point on the j-th camera.
 * Notice that depending on idxij, some of the hx_ij might be missing
 *
 */
void img_projsS_x ( double *p, struct sba_crsm *idxij, sint *rcidxs, sint *rcsubs, double *hx, void *adata )
{
// Jordi Ferrer, 02/02/2009
  double t[3];
// Jordi Ferrer, 02/02/2009
  sint i, j, cnp, pnp, mnp;
  double *pqr, *pt, *ppt, *pmeas, *Kcalib;
  // sint n;
  sint m, nnz;
  struct globs_ *gl;

  gl = (struct globs_ *)adata;
  Kcalib = gl->intrcalib;
  cnp = gl->cnp;  // 3 Rotation + 3 Translation Parametres
  pnp = gl->pnp;  // Euclidean 3D points
  mnp = gl->mnp;  // Imaged points are 4D
  pqr = gl->camparams;

  // n = idxij->nr;
  m = idxij->nc;

  for ( j = 0; j < m; ++j )
  {
    pt = pqr + 3;
    t[0] = pt[0] + Kcalib[5]; t[1] = pt[1] + Kcalib[6]; t[2] = pt[2] + Kcalib[7];

    nnz = sba_crsm_col_elmidxs ( idxij, j, rcidxs, rcsubs );  // Find nonzero hx_ij, i = 0..n-1

    for ( i = 0; i < nnz; ++i )
    {
      ppt = p + rcsubs[i] * pnp;
      pmeas = hx + idxij->val[rcidxs[i]] * mnp;        // Set pmeas to point to hx_ij

      calcImgProj ( Kcalib, pqr, pt, ppt, pmeas );     // Evaluate Q in pmeas
// Jordi Ferrer, 02/02/2009
      calcImgProj ( Kcalib, pqr, t, ppt, pmeas + 2 );  // Evaluate Q in pmeas
// Jordi Ferrer, 02/02/2009      
    }

    // j-th Camera Parameters
    pqr += cnp;
  }
}

/* Given a parameter vector p made up of the 3D coordinates of n points, compute in
 * jac the jacobian of the predicted measurements, i.e. the jacobian of the projections of 3D points in the m images.
 * The jacobian is returned in the order (B_11, ..., B_1m, ..., B_n1, ..., B_nm),
 * where B_ij=dx_ij/db_i (see HZ).
 * Notice that depending on idxij, some of the B_ij might be missing
 *
 */
void img_projsS_jac_x ( double *p, struct sba_crsm *idxij, sint *rcidxs, sint *rcsubs, double *jac, void *adata )
{
// Jordi Ferrer, 02/02/2009
  double t[3];
// Jordi Ferrer, 02/02/2009
  sint i, j, cnp, pnp, mnp;
  double *pqr, *pt, *ppt, *pB, *Kcalib;
  // sint n;
  sint m, nnz, Bsz, idx;
  struct globs_ *gl;

  gl = (struct globs_ *)adata;
  Kcalib = gl->intrcalib;
  cnp = gl->cnp;  // 3 Rotation + 3 Translation Parametres
  pnp = gl->pnp;  // Euclidean 3D points
  mnp = gl->mnp;  // Imaged points are 4D
  pqr = gl->camparams;

  // n = idxij->nr;
  m = idxij->nc;
  Bsz = mnp * pnp;

  for ( j = 0; j < m; ++j )
  {
    pt = pqr + 3;
    t[0] = pt[0] + Kcalib[5]; t[1] = pt[1] + Kcalib[6]; t[2] = pt[2] + Kcalib[7];

    nnz = sba_crsm_col_elmidxs ( idxij, j, rcidxs, rcsubs );  // Find nonzero hx_ij, i = 0..n-1

    for ( i = 0; i < nnz; ++i )
    {
      ppt = p + rcsubs[i] * pnp;
      idx = idxij->val[rcidxs[i]];
      pB = jac + idx * Bsz;  // Set pB to point to B_ij

      calcImgProjJacS ( Kcalib, pqr, pt, ppt, (double (*)[3])pB );       // Evaluate dQ/db in pB
// Jordi Ferrer, 02/02/2009
      calcImgProjJacS ( Kcalib, pqr, t, ppt, (double (*)[3])(pB + 6) );  // Evaluate dQ/db in pB
// Jordi Ferrer, 02/02/2009
    }

    // j-th Camera Parameters
    pqr += cnp;
  }
}



// Not Ready for now
//
void img_projTODO ( )
{
  fprintf ( stderr, "Not implemented yet!" );
  assert ( 1 );
}

void img_projKRTS(sint j, sint i, double *aj, double *bi, double *xij, void *adata)                               { img_projTODO ( ); }
void img_projKRTS_jac(sint j, sint i, double *aj, double *bi, double *Aij, double *Bij, void *adata)              { img_projTODO ( ); }
void img_projKRT(sint j, sint i, double *aj, double *xij, void *adata)                                            { img_projTODO ( ); }
void img_projKRT_jac(sint j, sint i, double *aj, double *Aij, void *adata)                                        { img_projTODO ( ); }
void img_projKS(sint j, sint i, double *bi, double *xij, void *adata)                                             { img_projTODO ( ); }
void img_projKS_jac(sint j, sint i, double *bi, double *Bij, void *adata)                                         { img_projTODO ( ); }
void img_projsKRTS_x(double *p, struct sba_crsm *idxij, sint *rcidxs, sint *rcsubs, double *hx, void *adata)      { img_projTODO ( ); }
void img_projsKRTS_jac_x(double *p, struct sba_crsm *idxij, sint *rcidxs, sint *rcsubs, double *jac, void *adata) { img_projTODO ( ); }
void img_projsKRT_x(double *p, struct sba_crsm *idxij, sint *rcidxs, sint *rcsubs, double *hx, void *adata)       { img_projTODO ( ); }
void img_projsKRT_jac_x(double *p, struct sba_crsm *idxij, sint *rcidxs, sint *rcsubs, double *jac, void *adata)  { img_projTODO ( ); }
void img_projsKS_x(double *p, struct sba_crsm *idxij, sint *rcidxs, sint *rcsubs, double *hx, void *adata)        { img_projTODO ( ); }
void img_projsKS_jac_x(double *p, struct sba_crsm *idxij, sint *rcidxs, sint *rcsubs, double *jac, void *adata)   { img_projTODO ( ); }
