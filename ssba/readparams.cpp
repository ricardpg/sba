/* Loading of camera, 3D point & image projection parameters from disk files */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sba_types.h>

#define MAXSTRLEN  4096 /* 4K */

static char buf[MAXSTRLEN];
/* get rid of the rest of a line upto \n or EOF */
#define SKIP_LINE(f){                                                       \
  while(!feof(f))                                                           \
    if(!fgets(buf, MAXSTRLEN-1, f) || buf[strlen(buf)-1]=='\n') break;      \
}

/* returns the number of cameras defined in a camera parameters file.
 * Each line of the file corresponds to the parameters of a single camera
 */
sint readNcameras ( FILE *fp )
{
sint lineno, ncams, ch;

  lineno=ncams=0;
  while(!feof(fp)){
    if((ch=fgetc(fp))=='#'){ /* skip comments */
      SKIP_LINE(fp);
      ++lineno;
      continue;
    }

    if(feof(fp)) break;

    SKIP_LINE(fp);
    ++lineno;
    if(ferror(fp)){
      fprintf(stderr, "readNcameras(): error reading input file, line %ld\n", lineno);
      return 0;
    }
    ++ncams;
  }

  return ncams;
}

/* reads into "params" the camera parameters defined in a camera parameters file.
 * "params" is assumed to point to sufficiently large memory.
 * Each line contains the parameters of a single camera, 7 parameters per camera are
 * assumed
 */
bool readCameraParams ( FILE *fp, double *params )
{
  const sint cnp=7;
  sint lineno, n, ch;

  lineno=0;
  while(!feof(fp)){
    if((ch=fgetc(fp))=='#'){ /* skip comments */
      SKIP_LINE(fp);
      ++lineno;
      continue;
    }

    if(feof(fp)) break;

    ungetc(ch, fp);
    ++lineno;
    n=fscanf(fp, "%lf%lf%lf%lf%lf%lf%lf\n", params, params+1, params+2, params+3, params+4, params+5, params+6); 
    if(n!=cnp){
      fprintf(stderr, "readCameraParams(): line %ld contains %ld parameters, expected %ld!\n", lineno, n, cnp);
      return false;
    }
    if(ferror(fp)){
      fprintf(stderr, "readNcameras(): error reading input file, line %ld\n", lineno);
      return false;
    }

    params+=cnp;
  }
  
  return true;
}


/* determines the number of 3D points contained in a points parameter file as well as the
 * total number of their 2D image projections across all images. The file format is
 * X Y Z  nframes  frame0 x0 y0  frame1 x1 y1 ...
 */
bool readNpointsAndNprojections ( FILE *fp, sint *n3Dpts, sint *nprojs )
{
  sint lineno, npts, nframes, ch;

  *n3Dpts=*nprojs=lineno=npts=0;
  while(!feof(fp)){
    if((ch=fgetc(fp))=='#'){ /* skip comments */
      SKIP_LINE(fp);
      ++lineno;
      continue;
    }

    if(feof(fp)) break;

    ungetc(ch, fp);
    ++lineno;
    fscanf(fp, "%*g%*g%*g%ld", &nframes);
    if(ferror(fp)){
      fprintf(stderr, "readNpointsAndNprojections(): error reading input file, line %ld\n", lineno);
      return false;
    }
    SKIP_LINE(fp);
    *nprojs+=nframes;
    ++npts;
  }

  *n3Dpts=npts;

  return true;
}


/* reads a points parameter file.
 * "params", "projs" & "vmask" are assumed preallocated, pointing to
 * memory blocks large enough to hold the parameters of 3D points, 
 * their projections in all images and the point visibility mask, respectively.
 * File format is X Y Z  nframes  frame0 x0 y0  frame1 x1 y1 ...
 */
bool readPointParamsAndProjections ( FILE *fp, double *params, double *projs, char *vmask, sint ncams )
{
  sint nframes, ch, lineno, ptno, frameno, n;
  register sint i;

  lineno=ptno=0;
  while(!feof(fp)){
    if((ch=fgetc(fp))=='#'){ /* skip comments */
      SKIP_LINE(fp);
      lineno++;

      continue;
    }

    if(feof(fp)) break;

    ungetc(ch, fp);

    fscanf(fp, "%lf%lf%lf", params, params+1, params+2); /* read in X Y Z */
    params+=3;

    fscanf(fp, "%ld", &nframes); /* read in number of image projections */

    for(i=0; i<nframes; ++i){
      n=fscanf(fp, "%ld%lf%lf%lf%lf", &frameno, projs, projs+1, projs+2, projs+3); /* read in image projection */
      if(n!=5){
        fprintf(stderr, "readPointParamsAndProjections(): error reading image projections from line %ld [n=%ld]\n", lineno+1, n);
        return false;
      }
      projs+=4;
      vmask[ptno*ncams+frameno]=1;
    }

    fscanf(fp, "\n"); // consume trailing newline

    lineno++;
    ptno++;
  }

  return true;
}


/* combines the above routines to read the initial estimates of the motion + structure parameters from text files.
 * Also, it loads the projections of 3D points across images. The routine dynamically allocates the required amount
 * of memory (last 3 arguments).
 */
bool readInitialSSBAEstimate(char *camsfname, char *ptsfname, sint *ncams, sint *n3Dpts, sint *n2Dprojs, double **motstruct, double **imgpts, char **vmask)
{
  const sint cnp=7, /* 4 rot params + 3 trans params */
            pnp=3, /* euclidean 3D points */
            mnp=4; /* img ponts are 4D */

  FILE *fpc, *fpp;
  bool Res;

  if ( ( fpc = fopen ( camsfname, "r" ) ) != NULL)
  {
    if ( ( fpp = fopen ( ptsfname, "r" ) ) != NULL )
    {
      *ncams = readNcameras ( fpc );
      readNpointsAndNprojections ( fpp, n3Dpts, n2Dprojs );

      *motstruct = (double *)malloc((*ncams*cnp + *n3Dpts*pnp)*sizeof(double));
      if ( *motstruct != NULL )
      {
        *imgpts = (double *)malloc(*n2Dprojs*mnp*sizeof(double));
        if ( *imgpts != NULL )
        {
          *vmask=(char *)malloc(*n3Dpts * *ncams * sizeof(char));
          if ( *vmask != NULL )
          {
            memset ( *vmask, 0, *n3Dpts * *ncams * sizeof(char)); /* clear vmask */

            /* prepare for re-reading files */
            rewind ( fpc );
            rewind ( fpp );

            if ( Res = readCameraParams ( fpc, *motstruct ) )
              Res = readPointParamsAndProjections ( fpp, *motstruct+*ncams*cnp, *imgpts, *vmask, *ncams );
          }
          else
          {
            fprintf(stderr, "memory allocation failed in readInitialSBAEstimate()\n");
            Res = false;
          }
        }
        else
        {
          fprintf(stderr, "memory allocation failed in readInitialSBAEstimate()\n");
          Res = false;
        }
      }
      else
      {
        fprintf(stderr, "memory allocation failed in readInitialSBAEstimate()\n");
        Res = false;
      }

      // Close opened files
      fclose ( fpc );
      fclose ( fpp );
    }
    else
    {
      fprintf ( stderr, "Cannot open file %s, exiting\n", ptsfname );
      fclose ( fpc );
      Res = false;
    }
  }
  else
  {
    fprintf ( stderr, "Cannot open file %s, exiting\n", camsfname );
    return false;
  }

  return Res;
}

/* reads the 3x3 + 1x3 intrinsic calibration matrix contained in a file */
bool readSCalibParams(char *fname, double ical[9+3])
{
  FILE *fp;
  bool Res;

  if ( ( fp = fopen ( fname, "r" ) ) != NULL )
  {
    sint i;
    for ( i = 0; i < 4; i++ )
    {
      fscanf ( fp, "%lf%lf%lf\n", ical, ical+1, ical+2 );
      ical += 3;
    }

    fclose(fp);
    Res = true;
  }
  else
    Res = false;

  return Res;
}

void printMask ( const char *FileName, const char *vmask, const sint ncams, const sint n3Dpts )
{
  sint i, j;

  FILE *f;

  if ( ( f = fopen ( FileName, "w" ) ) == NULL )
  {
    fprintf ( stderr, "printMask ( ): File '%s' cannot be open!\n", FileName );
    return;
  }

  fprintf ( f, "Print visibility mask (Rows,Columns) = (n3Dpts x ncams) = (%ld x %ld):\n\n", n3Dpts, ncams );

  // Space for the title column
  fprintf ( f, "    " );

  // Caption row
  for ( j = 0; j < ncams; j++ )
    fprintf ( f, " %3d", j );
  fprintf ( f, "\n" );

  for ( i = 0; i < n3Dpts; i++ )
  {
    fprintf ( f, "%3d. ", i );
    for ( j = 0; j < ncams; j++ )
    {
      fprintf ( f, "%3d ", vmask[i * ncams + j] );
    }
    fprintf ( f, "\n" );
  }

  fclose ( f );
}

/* prints the initial estimates of the motion + structure parameters. It also prints the projections
 * of 3D points across images. For debugging purposes only.
 */
void printSSBAData(char *MotionFileName, char *StructFileName, char *ProjectionsFileName,
                  double *motstruct, sint ncams, sint n3Dpts, double *imgpts, sint n2Dprojs, char *vmask)
{
const sint cnp=7, /* 4 rot params + 3 trans params */
          pnp=3, /* euclidean 3D points */
          mnp=4; /* img ponts are 4D */

register sint i;
FILE *f = fopen(MotionFileName, "w");
 //printf("Motion parameters:\n");
 for(i=0; i<ncams*cnp; ++i)
 {
   fprintf(f, "%lf", motstruct[i]);
   if ((i+1)%7==0)
     fprintf(f,"\n");
   else
     fprintf(f," ");
 }
 fclose(f);

 f =fopen(StructFileName, "w");
 motstruct+=i;
 //printf("\n\nStructure parameters:\n");
 for(i=0; i<n3Dpts*pnp; ++i)
 {
   fprintf(f,"%lf", motstruct[i]);
   if ((i+1)%3==0)
     fprintf(f,"\n");
   else
     fprintf(f," ");
 }
 fclose(f);

 f =fopen(ProjectionsFileName, "w");
 //printf("\n\nImage projections:\n");
 for(i=0; i<n2Dprojs*mnp; ++i)
 {
   fprintf(f,"%lf", imgpts[i]);
   if ((i+1)%4==0)
     fprintf(f,"\n");
   else
     fprintf(f," ");
 }
 fclose(f);
}
