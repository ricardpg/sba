%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Stereo Sparse Bundle Adjustement reading data.
%
%  File          : ssbaRead.m
%  Date          : 15/10/2007 - 15/10/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  ssbaRead Read the data for the Monocular Sparse Bundle Adjustement.
%
%      [Calibration, Cameras, Points] = ssbaRead ( CalibrationFileName, ...
%                                                  CamerasFileName, PointsFileName )
%
%     Input Parameters:
%      CalibrationFileName: String containing the file path which contains the
%                           calibration data.
%      CamerasFileName: String containing the file path which contains the
%                       camera poses.
%      PointsFileName: String containing the file paht which contains the
%                      3D points and where they are imaged.
%
%     Output Parameters:
%      Calibartion: 3x3 matrix containing the intrinsic parameters matrix.
%      Cameras: 7xN matrix containing the camera poses in format
%               (qr, q1, q2, q3, X, Y, Z).
%      Points: See msba.m
%
%

function [Calibration, Cameras, Points] = ssbaRead ( CalibrationFileName, ...
                                                     CamerasFileName, PointsFileName )
  % Test the input parameters
  error ( nargchk ( 3, 3, nargin ) );
  error ( nargoutchk ( 3, 3, nargout ) );

  % Test the filenames
  if ~ischar ( CalibrationFileName );
    error ( 'MATLAB:ssbaReadPoints:Input', 'CalibrationFileName must be a string!' );
  end
  if ~ischar ( CamerasFileName );
    error ( 'MATLAB:ssbaReadPoints:Input', 'CamerasFileName must be a string!' );
  end
  if ~ischar ( PointsFileName );
    error ( 'MATLAB:ssbaReadPoints:Input', 'PointsFileName must be a string!' );
  end

  [Calibration, Cameras, Points ] = ssbaReadMx ( CalibrationFileName, ...
                                                 CamerasFileName, PointsFileName );
end

