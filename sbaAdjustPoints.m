%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Adjust the Sparse Bundle Adjustement Points structure.
%
%  File          : sbaAdjustPoints.m
%  Date          : 30/01/2007 - 12/01/2009
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2009 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  sbaAdjustPoints Reduce each possible set of 3D points to one.
%
%      PointsR = sbaAdjustPoints ( Points, PickFirst, IsStereo )
%
%     Input Parameters:
%      Points: See msba.m
%      PickFirst: Optional parameter to use the first occurrence instead of
%                 the mean.
%      IsStereo: If true, the Stereo is taken into account otherwise
%                monouclar case is assumed. In Stereo, Points structure
%                must have 2 x 2D points in the rows of the cell.
%
%     Output Parameters:
%      Points: Same structure than Points without the multiplicity in the
%              3D Point set.
%
%

function PointsR = sbaAdjustPoints ( Points, PickFirst, IsStereo )
  % Test the input parameters
  error ( nargchk ( 3, 3, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % Check the msba/ssba Points Structure content allowing multiple 3D points.
  sbaCheckPoints ( Points, true, IsStereo );
  [Cr, numpts3D] = size ( Points );

  % Copy the structure (as there shouldn't be too much 3D point multiplicity, 
  % is better to use Matlab to copy insead of using a "for" sentence).
  PointsR = Points;

  for i = 1 : numpts3D;
    if PickFirst;
      PointsR{1, i} = PointsR{1, i}(:, 1);
    else
      PointsR{1, i} = mean ( PointsR{1, i}, 2 );
%      PointsR{1, i} = median ( PointsR{1, i}, 2 );
    end
  end
end
