#ifndef __SBA_TYPES_H__
#define __SBA_TYPES_H__

/* Decide whether to use a 64 bit int or a 32 bit int (comment the line) */
// #define SBA_DO_USE_INT64

/* Decide between LLP64 and LP64 Platforms */
#if defined (WINDOWS) || defined (WIN32)
  /* LLP64 Platforms (Windows, Visual Studio) */
  typedef long long int int64;
  typedef unsigned long long int uint64
#else
  /* LP64 Platforms (Solaris, AIX, HP, Linux, Mac OS X, FreeBSD, and IBM z/OS) */
  typedef long int int64;
  typedef unsigned long int uint64;
#endif

#ifdef SBA_DO_USE_INT64
  typedef int64 sint;
  typedef uint64 suint;
#else
  typedef int sint;
  typedef unsigned int suint;
#endif

#endif
