%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Adjust the Sparse Bundle Adjustement Points structure.
%
%  File          : sbaAdjustPointsCenter.m
%  Date          : 09/05/2008 - 21/05/2008
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2008 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  sbaAdjustPointsCenter Reduce each possible set of 3D points to one by selecting
%                        the estimation closer to the image center.
%
%      PointsR = sbaAdjustPointsCenter ( Points, ImgSize, IsStereo )
%
%     Input Parameters:
%      Points: See msba.m
%      ImgSize: 1x2 vector containg the image size (rows, columns).
%      IsStereo: If true, the Stereo is taken into account otherwise
%                monouclar case is assumed. In Stereo, Points structure
%                must have 2 x 2D points in the rows of the cell.
%
%     Output Parameters:
%      Points: Same structure than Points without the multiplicity in the
%              3D Point set.
%
%

function PointsR = sbaAdjustPointsCenter ( Points, ImgSize, IsStereo )
  % Test the input parameters
  error ( nargchk ( 3, 3, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % Check the msba/ssba Points Structure content allowing multiple 3D points.
  sbaCheckPoints ( Points, true, IsStereo );
  [Cr, numpts3D] = size ( Points );

  if ~all ( size ( ImgSize ) == [ 1 2 ] );
    error ( 'MATLAB:sbaAdjustPointsCenter:Input', ...
            'Input vector ImgSize must be a 1x2 vector containing the image size!' );
  end

  % Copy the structure (as there shouldn't be too much 3D point multiplicity, 
  % is better to use Matlab to copy insead of using a "for" sentence).
  PointsR = Points;
  Center = [ ImgSize(2) ImgSize(1) ]';
  for i = 1 : numpts3D;
    % Calculate the image centers vector
    Centers = repmat ( Center, 1, size ( PointsR{2, i}, 2 ) );
    Dl = PointsR{2, i}(2:3,:) - Centers;
    if IsStereo;
      % Calculate individual distances
      Dr = PointsR{2, i}(4:5,:) - Centers;
      Ds = [ Dl; Dr ];
      D = sqrt ( sum ( Ds .* Ds ) );
    else
      % Calculate individual distances
      D = sqrt ( sum ( Dl .* Dl ) );
    end

    % Select the smallest distance
    [v, Idx] = min ( D );
    % Select the 3D Point
    PointsR{1, i} = PointsR{1, i}(:, Idx);
  end
end
