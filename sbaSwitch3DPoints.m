%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Function to substitute the 3D set of points in the
%                  Bundle Adjustement Point structure.
%
%  File          : sbaSwitch3DPoints.m
%  Date          : 08/02/2007 - 12/10/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  sbaSwitch3DPoints Function to substitute the 3D set of points in the 
%                    Sparse Bundle Adjustement structure for a given set.
%
%      PointsR = sbaSwitch3DPoints ( Points, Points3D )
%
%     Input Parameters:
%      Points: See msba.m
%      Points3D: 3xN set of 3D points in X,Y,Z format.
%      IsStereo: If true, the Stereo is taken into account otherwise
%                monouclar case is assumed. In Stereo, Points structure
%                must have 2 x 2D points in the rows of the cell.
%
%     Output Parameters:
%      PointsR: Points structure with the set of 3D points substituted by
%               Points3D.
%
function PointsR = sbaSwitch3DPoints ( Points, Points3D, IsStereo )
  % Test the input parameters
  error ( nargchk ( 3, 3, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % Check the Points Structure Content
  sbaCheckPoints ( Points, false, IsStereo );
  [Cr, numpts3D] = size ( Points );

  % Check the colors for the points
  [CPr, CPc] = size ( Points3D );
  if CPr ~= 3 || CPc ~= numpts3D;
    error ( 'MATLAB:sbaSwitch3DPoints:Input', ...
            'Points must be a 3xM set of 3D points with M equal to the Points number of columns!' );
  end

  % Copy the Initial Structure
  PointsR = Points;

  % Replace the 3D points
  for i = 1 : numpts3D; PointsR{1,i} = Points3D(:,i); end
end
