%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Compile everything.
%
%  File          : Compile.m
%  Date          : 19/02/2007 - 27/01/2009
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%                  - This script should be run in Matlab from the STracker/
%                    folder.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2008 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%

% Detect Matlab Version
MatVer = version ( '-release' );

% 13 for 6.5 or smaller for lower versions and 20 for others (2006a, 2006b, 2007a, 2007b, 2008a)
Major = str2double ( MatVer(1:2) );

% Detect Platform
Arch = computer;

if strcmpi ( Arch, 'pcwin' ); Windows = 1; Nbit = 32;
elseif strcmpi ( Arch, 'pcwin64' ); Windows = 1; Nbit = 64;
else Windows = 0;
end



if Windows;
  warning ( 'MATLAB:Compile:Check', ...
            [ 'For windows users using Visual Studio, they must execute ''vcvars??.bat''\n' ...
              'to set the environament vairables. Please read ''README'' file for manual\n' ...
              'compilation using the command line!\n' ] );
  disp ( 'RETURN to continue or CTRL+C to stop!' );
  pause;
end

disp ( 'Compiling system binaries...' );
disp ( '  Sba library...' );
cd 3rdParty/sba-1.5/
if Windows;
  if Nbit == 32;
    !nmake -f Makefile.vc32
  elseif Nbit == 64;
    !nmake -f Makefile.vc64
  end
else
  !make
end
cd ../../

disp ( '  msba binary...' );
cd msba/bin/
if Windows;
  if Nbit == 32;
    !nmake -f Makefile.vc32
  elseif Nbit == 64;
    !nmake -f Makefile.vc64
  end
else
  !make
end
cd ../../

disp ( '  ssba binary...' );
cd ssba/bin/
if Windows;
  if Nbit == 32;
    !nmake -f Makefile.vc32
  elseif Nbit == 64;
    !nmake -f Makefile.vc64
  end
else
  !make
end
cd ../../

disp ( '  msba... TO DO!!!' );
disp ( '  msba...' );
cd msba/
if Major <= 13; Compile65; else Compile; end
cd ../

disp ( '  ssba...' );
cd ssba/
if Major <= 13; Compile65; else Compile; end
cd ../



disp ( 'Executing system binary tests...' );
disp ( '  msba...' );
cd msba/bin/Test/
if Windows;
  !nmake -f Makefile.vc
else
  !make
end
cd ../../../

disp ( '  ssba...' );
cd ssba/bin/Test/
if Windows;
  !nmake -f Makefile.vc
else
  !make
end
cd ../../../
