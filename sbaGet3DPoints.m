%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Extract the 3D points from the Sparse Bundle Adjustement
%                  Points structure.
%
%  File          : sbaGet3DPoints.m
%  Date          : 17/01/2007 - 13/02/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  sbaGet3DPoints Extract the 3D points from the Monocular Sparse Bundle
%                 Adjustement Points structure that contains of 3D points
%                 and the imaged set of 2D points.
%
%      Points3D = sbaGet3DPoints ( Points, IsStereo )
%
%     Input Parameters:
%      Points: See msba.m
%      IsStereo: If true, the Stereo is taken into account otherwise
%                monouclar case is assumed. In Stereo, Points structure
%                must have 2 x 2D points in the rows of the cell.
%
%     Output Parameters:
%      Points3D: 3xM set of 3D points corresponding the first row in the
%                cell.
%
%

function Points3D = sbaGet3DPoints ( Points, IsStereo )
  % Test the input parameters
  error ( nargchk ( 2, 2, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % Check the msba/ssba Points structure content
  sbaCheckPoints ( Points, false, IsStereo );
  [Cr, numpts3D] = size ( Points );

  % Copy the points
  Points3D = zeros ( 3, numpts3D );
  for i = 1:numpts3D;
    Points3D(:,i) = Points{1,i};
  end
end
