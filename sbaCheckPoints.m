%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Check Sparse Bundle Adjustement Points Structure content.
%
%  File          : sbaCheckPoints.m
%  Date          : 22/01/2007 - 12/10/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  sbaCheckPoints Check whether the content of the msba/ssba Point structure
%                 is correct or not.
%
%      sbaCheckPoints ( Points, Mult3D, IsStereo )
%
%     Input Parameters:
%      Points: See msba.m
%      Mult3D: Allow multiple 3D points for each column in the point
%              structure. In the case of multiple 3D points, they must be
%              stored in columns.
%      IsStereo: If true, the Stereo is taken into account otherwise
%                monouclar case is assumed. In Stereo, Points structure
%                must have 2 x 2D points in the rows of the cell.
%
%     Output Parameters:
%

function sbaCheckPoints ( Points, Mult3D, IsStereo )
  % Test the input parameters
  error ( nargchk ( 3, 3, nargin ) );
  error ( nargoutchk ( 0, 0, nargout ) );

  if ~islogical ( Mult3D );
    error ( 'MATLAB:sbaCheckPoints:Input', 'Mult3D must be a logical!' );
  end

  if ~islogical ( IsStereo );
    error ( 'MATLAB:sbaCheckPoints:Input', 'Mult3D must be a logical!' );
  end

  % Compose error string
  InitStr = 'Points must be a 2xK cell-array containing on each column:\n';
  if IsStereo;
    NumRowsAllowed = 1 + 2 * 2;
    MultStr = '{ (X,Y,Z) N times; (Frame Number, X-2D, Y-2D, X''-2D, Y''-2D) N times }!';
    NoMultStr = '{ (X,Y,Z); (Frame Number, X-2D, Y-2D, X''-2D, Y''-2D) N Times }!';
  else
    NumRowsAllowed = 1 + 2 * 1;
    MultStr = '{ (X,Y,Z) N times; (Frame Number, X-2D, Y-2D) N times }!';
    NoMultStr = '{ (X,Y,Z); (Frame Number, X-2D, Y-2D) N Times }!';
  end

  if Mult3D; ErrorStr = [ InitStr MultStr ]; else ErrorStr = [ InitStr NoMultStr ]; end

  [Cr, numpts3D] = size ( Points );
  if ~iscell ( Points ) || Cr ~= 2 || numpts3D <= 0;
    error ( 'MATLAB:sbaCheckPoints:Input', ErrorStr );
  end

  % Check the content
  for i = 1 : numpts3D;
    if ~isempty ( Points{1,i} ) && ~isempty ( Points{2,i} ) ;
      [Rows1, Columns1] = size ( Points{1,i} );
      [Rows2, Columns2] = size ( Points{2,i} );

      % Test the number of 3D points.
      if Mult3D;
        % Allowing more than one 3D point.
        if Rows1 ~= 3 || Columns1 <= 0;
          error ( 'MATLAB:sbaCheckPoints:Input', ErrorStr );
        end

        % Test when allowing more than one 3D points
        if Columns1 ~= Columns2 && Columns1 ~= 1;
          error ( 'MATLAB:sbaCheckPoints:Input', ErrorStr );
        end
      else
        % Only 1 3D points is allowed.
        if ~( Rows1 == 1 && Columns1 == 3 ) && ~( Rows1 == 3 && Columns1 == 1 );
          error ( 'MATLAB:sbaCheckPoints:Input', ErrorStr );
        end
      end

      % Test the number of 2D points.
      if Rows2 ~= NumRowsAllowed || Columns2 <= 0;
        error ( 'MATLAB:sbaCheckPoints:Input', ErrorStr );
      end
    else
      error ( 'MATLAB:sbaCheckPoints:Input', ErrorStr );
    end
  end  
end
