%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Adjust a Color structure for the Sparse Bundle Adjustement.
%
%  File          : sbaAdjustColors.m
%  Date          : 08/02/2007 - 12/10/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  sbaAdjustPoints Reduce each possible set of colors to one.
%
%      ColorsR = sbaAdjustColors ( Colors, PickFirst )
%
%     Input Parameters:
%      Colors: 1xN Cell-array containing 3xM sets of R, G, B Colors.
%      PickFirst: Optional parameter to use the first occurrence instead of
%                 the mean.
%
%     Output Parameters:
%      ColorsR: 1xN Cell-array containing 3x1 R, G, B Colors.
%
%

function ColorsR = sbaAdjustColors ( Colors, PickFirst )
  % Test the input parameters
  error ( nargchk ( 2, 2, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % Check the Color Structure content allowing multiple RGB points.
  [Cr, NumColors] = size ( Colors );
  if ~iscell ( Colors ) || Cr ~= 1 || NumColors <= 0;
    error ( 'MATLAB:sbaAdjustColors:Input', ...
            'Colors must be a 1xN cell array containing 3xM (R, G, B) vectors in rows for each 3D point!' );
  end

  % Copy the structure (as there shouldn't be too much RGB multiplicity,
  % is better to use Matlab to copy insead of using a "for" sentence).
  ColorsR = Colors;

  for i = 1 : NumColors;
    if PickFirst;
      ColorsR{1, i} = ColorsR{1, i}(:, 1);
    else
      ColorsR{1, i} = mean ( ColorsR{1, i}, 2 );
    end
  end
end
